# Purpose of the functionality

*Here a description of the functionality shall be added in "current language"*

# Functions/procedures to be added

- [ ] **brief description**
    - expected inputs
    - expected outputs
- [ ] **creation of function**
    - [ ] code of function
    - [ ] documentation of function
    - [ ] example for function
    - [ ] test for function

# Data to be added

**Warning**: To add data, there is a need to add script to allow maintenance of data

- [ ] **brief description**
    - source of the data
    - storage of the data (inst/extdata)
- [ ] **creation of data**
    - [ ] script for creation of "raw" data (if needed)
    - [ ] storage of raw data in ascii format, normally in inst/extdata
    - [ ] script in to maintain data in /data-raw
    - [ ] documentation of data

