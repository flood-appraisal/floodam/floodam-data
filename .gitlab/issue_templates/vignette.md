# Purpose of the vignette

*Here a description of the vignette shall be added*

## Documentation of data

- [ ] **original data**
    - [ ] who's in the charge oif the data
    - [ ] what is the data about
    - [ ] how is the data organized
    - [ ] where is the data available
    - [ ] demonstration of downloading data
- [ ] **adapted data**
    - [ ] how is the data organized
    - [ ] what are the adaptations done
    - [ ] demonstration of adapting data
- [ ] **analysis**
    - [ ] what is intersting about the data
    - [ ] demonstrations of how to perform analysis
- [ ] **maintenance**
    - [ ] how can be performed an automatic maintenance
