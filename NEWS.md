# floodam.data 0.9.43.0

## Enhancements

* `adapt_geo_sirene()`
    * utilisation de `read_rds_safe()`
* `analyse_archive()`
    * better analysis of date when `origin` is "floodam.data"
* `read_rds_safe()` (new)
    * read a rds file safeley, returning `NULL` if nothing is found

# floodam.data 0.9.42.0

## Enhancements

* `alert_mattermost()`
    * new beahviour when `alert` is `FALSE`: return the command that should be
    passed to `system()`
    * document updated
* `alert_mattermost_lib()` (new)
    * send a message to a Mattermost server when a library has been updated,
    based on `alert_mattermost()`
    * documentation added
* `interpret_args()` (new)
    * transform results from `commandArgs()` in a named list for script
    parameters written as 'param=value', should be helpful for scripts written
    around `floodam.data` for maintenance purposes
    * documentation & tests added

# floodam.data 0.9.41.0

## Enhancements

* `adapt_geo_sirene()` (rename)
    * new name for `adapat.geo_sirene`
    * process changed to fit with `add_log_info()` and use of what classical
    steps should be for all data (read with a scheme, select observations, split
    by departments if necessary, add eaip information, save data and plot a map)
    * documentation updated
* `add_eaip()` (internal)
    * messages added to better fllow analysis
* `add_log_info()`
    * better generation of sub-messages for analysis
* `analyse_archive()`
    * new `origin` for "geo-sirene"
    * documentation & tests updated
* `apply_scheme()`
    * `Date` is used to detect when to apply `as.Date()` (consistency)
    * `logical` type is detected and transformed with `to_logical()`
* `download_archive()`
    * adaptation to `get_archive()` (removal of parameter `html`, inclusion of
    field `local_origin` in `to_do` parameter)
    * field `short` in `to_do` may be missing
    * documentation updated
* `download_ban()`
    * adaptation to `get_link_from_html()`
    * introduction of parameter `repository`, different from `origin` (only url)
    * simplification of process
    * documentation updated (markdown format)
* `download_geo_sirene()` (rename)
    * new name for `download.geo_sirene`
    * introduction of parameter `repository`, different from `origin` (only url)
    * introduction of parameter `scope`, used when `repository` is "data.gouv"
    * simplification of process
    * documentation updated (markdown format)
* `format_journal()` (internal)
    * enhanced format dates coming from `base::Sys.Date()`
* `get_archive()`
    * adaptation to `get_link_from_html()`
    * simplification and introduction of `local_origin` to deal with relative
    links
    * documentation updated
* `get_base_url()` (new)
    * return the parent url from a url
    * documentation & tests added
* `get_date_from_html()` (new)
    * find admissible dates in a html page
    * documentation & tests added
* `get_link_from_html()` (new)
    * find links in a html page
    * documentation & tests added
* `read_csv_with_scheme()`
    * `Date` is used to detect when to apply `as.Date()` (consistency)
    * `logical` type is detected and transformed with `to_logical()`
    * preprocessing to remove observations with `NA` in lon\lat before
    transformation with `sf::st_as_sf()`
    * messages generation added
* `scheme_bd_topo_3` (data)
    * complete missing `type_source`
* `scheme_gaspar_azi` (data)
    * correct value for `type` and `type_source`
* `scheme_gaspar_catnat` (data)
    * correct value for `type` and `type_source`
* `scheme_gaspar_dicrim` (data)
    * correct value for `type` and `type_source`
* `scheme_gaspar_pcs` (data)
    * correct value for `type` and `type_source`
* `scheme_gaspar_pprn` (data)
    * correct value for `type` and `type_source`
* `scheme_gaspar_risq` (data)
    * correct value for `type_source`
* `scheme_rpg_1` (data)
    * correct value for `type_source`
* `scheme_rpg_1` (data)
    * correct value for `type_source`
* `scheme_sirene_2019` (data)
    * style of field names "." → "_"
    * "longitude" and "latitude" renamed to "lon" and "lat" in `name` field for
    consistency and usage in `read_csv_with_scheme()`
    * documentation updated
* `scheme_sirene_na` (data)
    * style of field names "." → "_"
    * documentation updated
* `subset_with_message()` (new)
    * subset without side-effects and with messages useful for `add_log_info()`
    * documentation added
* `split_dep()` (new, internal)
    * split a `sf` `data.frame `either geomatically if a department `sf` object
    is  given, or through the information given in `commune` if not
    * documentation added
* `to_logical()`
    * default treatment for easy English and French usage
    * documentation updated & tests added

# floodam.data 0.9.40.0

## Enhancements

* `download_ban()`
    * bug corrected when date is specified

# floodam.data 0.9.39.0

## Enhancements

* `adapt_admin_express()`
    * create `destination` if it does not exist

# floodam.data 0.9.38.0

## Enhancements

* `adapt.geo_sirene()`
    * use of `in_layer()` instead of `in_layer_old()` to have a better use
    `lwgeom::st_subdivide()`
* `adapt_rpg_()`
    * use of `in_layer()` instead of `in_layer_old()` to have a better use
    `lwgeom::st_subdivide()`
* `add_eaip()` (internal)
    * use of `in_layer()` instead of `in_layer_old()` to have a better use
    `lwgeom::st_subdivide()`
    * adaptation to call of `file_version()`
* `extract_building()`
    * use of `analyse_archive()` to get useful informations from archive
* `file_version()` (internal)
    * use recursive = TRUE to find a version (should be used in directory with
    subdirectories giving vintages).
* `generate_report()`
    * option `complete` added to have more detailed reports
* `map_log_ban()` (internal)
    * adaptation to call of `file_version()`
* `map_log_building()` (internal)
    * use reset = FALSE for first plot
* `map_log_dwelling()` (internal)
    * adaptation of pattern to find department version with `file_version()`
    * use reset = FALSE for first plot
* `map_log_dwelling_nc()` (internal)
    * adaptation of pattern to find department version with `file_version()`
    * use reset = FALSE for first plot
* `template_bd-topo_dwelling.Rmd` (template)
    * correction of title
* `template_bd-topo_dwelling_complete.Rmd` (template)
    * add some graphics to have a more complete view of dwellings'
    characteristics

# floodam.data 0.9.37.0

## Enhancements

* `download_archive()`
    * adapted to used `html` parameter in get_archive
* `download_ban()` (bug)
    * adapted to new way of presenting distant pages in data.gouv.fr
    * new analysis of date and introduction of `html` parameter in
    `download_archive`
* `get_archive()` 
    * new parameter `html` to deal with how html pages are readed from some
    distant sites
    * problem occurs when html uses javascript to build links instead of having
    them cleraly written
* `format_address()` (new)
    * create an address based on expected columns of a data.frame
    * documentation and test added
* `update_ban()` (new)
    * adapt a data.frame that has information coming from an old ban version to
    a new ban version, incorporating some given manual corrections
    * produce a data.frame of manual corrections to be performed
    * all observations in observation data.frame are kept in adapted
    observations
    * documentation and test added
* `update_state()` (new)
    * create a state from an observation data.frame and a ban data.frame
    * all observations in ban data.frame are kept in state data.frame
    * documentation and test added

## Documentation

* `adapt_ban()`


## Bugs

# floodam.data 0.9.36.0

## Enhancements

* nomenclature_clc
    * nomemclature for Corine Land Cover and some color codes

## Documentation

* _pkgdown.yml
    * adaptation to pkgdown 2.0.7

# floodam.data 0.9.35.0

## Enhancements

* `adapt_admin_express()`
    * replace `adapt.admin_express`
    * use `add_log_info()` for journal from new journal system
    * use `read_with_scheme()` to read and format data
    * use `save_archive()` to save result
    * option 'export' renamed in 'extension' for consistency with
    `save_archive()`
    * documentation updated
* `analyse_archive()`
    * function within its own file and exported
    * treat 2 formats ('ign' & 'floodam.data')
    * addition of warnings
    * function tested in test_analyse_archive
    * **TO DO: put information for format in a global variable**
* `format_archive()` (new)
    * permet de créer des noms formattés des archives à partir
    d'informations standardisées
    * origin = "floodam.data" (défault) pour créer les noms des archives de
    sauvegarde
    * origin = "ign" pour créer les noms à la façon IGN (inverse de
    analyse_archive)
    * origin = "version" pour créer des patterns pour chercher dans les noms
    façon IGN.
    * tests dans test_format_archive
* `save_archive()` (new)
    * sauvegarde d'une archive de façon standardisée
    * analyse le path pour trouver les informations (possibilité d'utiliser
    l'option origin pour spécifier si origin = "floodam.data" ou origin = 
    "ign")
    * extension peut-être écrasée par paramètre dédié
    * gestion du scope (attention ça peut-être un facteur qui découpe
    les données ou le nom d'une colonne dans cet input).
    * extension gérée pour le moment: rds
    * tests dans test_save_archive
* `downlaod_archive()`
    * renvoie un message si aucune archive n'est trouvée avec les options
    demandées
    * utilisation de add_journal_new au lieu de add_journal
* `add_journal()`
    * reprise de add_journal_new
    * ancien add_journal renommée temporairement en add_journal_old le
    temps de la migration
    * meilleure présentation des sous-tâches en introduisant le paramètre
    degree
* `write_journal()` (new)
    * permet d'écrire un journal produit par `add_journal_new()` soit au format
    "csv", soit au format "log" en fonction de l'extension du fichier
    * si aucun fichier n'est donné, le message au format "log" est affiché
    sur la sortie standard
    * si un fichier existant est donné, le journal est ajouté au précédent,
    quelque soit le format choisi ("log" ou "csv")
    * Non exportée
* `basename_core()` (new)
    * trouve le nom d'une archive en enlevant le path et les doubles
    extensions de type .csv.gz ou .7z.001 notamment
    * déploiement dans les fonctions
    * tests dans test_basename_utilities
*  `basename_ext()` (new)
    * trouve les extensions y compris les doubles de type .csv.gz ou .7z.001
    notamment
    * déploiements dans les fonctions
    * tests
* `format_journal()` (new)
    * permet de formatter un journal (ou une partie de journal) pour
    préparer des messages au format "log"
    * Non exportée
* `read_with_scheme()`
    * wrapper pour différentes fonctions plus spécifiques
    * gère csv, csv.gz, shp, gpkg, shp from 7z, gpkg from 7z, zip
    * pour zip, attention fait appel à une commande 'system' en modifiant
    le nom du fichier pour faire une commande qui sera détectée par
    `data.table::fread()`, et présume que le format sera csv et pourra être lu
    avec `read_csv_with_scheme()`
    * pour zip, modifie temporairemen l'option
    'datatable.fread.input.cmd.message' pour éviter le message renvoyé par
    `data.table::fread()`
* `read_csv_with_scheme()` (new)
    * correspond à ancienne version de `read_with_scheme()` qui était "csv"
    spécifique
* `read_gpkg_with_scheme()` (new)
    * gestion de gpkg avec scheme et projection
* `read_shp_with_scheme()`  (new)
    * gestion de shp avec scheme et projection
* `read_shp_from_7z()`
    * Ancienne version n'était pas finalisée
    * Extraction en fonction de la layer spécifiée
* `read_gpkg_from_7z()`
    * Séparation claire de la décompression et de la lecture
    * Sécurité si plusieurs gpkg 
* `download_admin_express()`
    * transformation de `download.admin_express`
    * meilleure gestion des options (et match.arg)
* `scheme_admin_express_3_1 `(data)
    * nouveau format à partir des infos IGN (version 3-1)
    * style des noms des variables "." → "_"
    * MaJ de data-raw/scheme.R et R/data.R pour intégration & documentation
* `scheme_admin_express_2_0` (data)
    * renommage de scheme_admin_express (data) qui ne faisait pas mention de
    la version (version 2-0)
    * style des noms des variables "." → "_"
    * MaJ de data-raw/scheme.R et R/data.R pour intégration & documentation
* `scheme_insee_2019 `(data)
    * renommage de certaines variables ('type_loge' en 'type', 'n_loge' en
    'dwelling', 'cat_loge' en 'usage') pour plus de cohérénces
* documentation
    * corrections de coquilles (à la volée) pour les datasets
    * regroupement des documentions pour fonction de la famille
    `read_with_scheme()`
* data.cquest.org
    * toutes les mentions à l'adresse passent en https au lieu de http
* Roxygen
    * utilisation de l'option markdown = TRUE (dans DESCRIPTION)

## Bugs
