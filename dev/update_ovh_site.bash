source ../.ovh_variable
LIB=$(head -n 1 DESCRIPTION | sed -r 's/Package[:] //')
R -e "pkgdown::build_site()"
sshpass -p "${OVH_PASS}" rsync -a -e "ssh -o StrictHostKeyChecking=no" docs/ ${OVH_LOGIN}@${OVH_SFTP}:/home/${OVH_LOGIN}/${LIB}
