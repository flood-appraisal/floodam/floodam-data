setwd("floodam.data")

category_label = data.table::fread("inst/extdata/category_label.csv", data.table = FALSE, colClasses = "character")
Encoding(category_label[["label.fr"]]) = "UTF-8"
usethis::use_data(category_label, overwrite = TRUE)

commune_so_ii = data.table::fread("inst/extdata/commune_so_ii.csv", data.table = FALSE)
Encoding(commune_so_ii[["commune_name"]]) = "UTF-8"
Encoding(commune_so_ii[["epci"]]) = "UTF-8"
usethis::use_data(commune_so_ii, overwrite = TRUE)

department = data.table::fread("inst/extdata/department.csv", data.table = FALSE, colClasses = "character")
rownames(department) = department[["code"]]
Encoding(department[["label.fr"]]) = "UTF-8"
usethis::use_data(department, overwrite = TRUE)

region = data.table::fread("inst/extdata/region.csv", data.table = FALSE, colClasses = "character")
rownames(region) = region[["code"]]
Encoding(region[["label.fr"]]) = "UTF-8"
usethis::use_data(region, overwrite = TRUE)

eaip_variable = c("ORIGINE", "IDENT", "DOCUMENT", "AUTEUR", "DATE_APPRO", "PRECISION", "THEME", "LIEN_RAPPO", "FOURNISS", "RETENU", "DATE_IMPOR", "geometry")
usethis::use_data(eaip_variable, overwrite = TRUE)

nace_label = data.table::fread("inst/extdata/nace_label.csv", data.table = FALSE,
    colClasses = c("integer", "character", "character", "character"))
Encoding(nace_label[["label.en"]]) = "UTF-8"
Encoding(nace_label[["label.fr"]]) = "UTF-8"
usethis::use_data(nace_label, overwrite = TRUE)

naf_label = data.table::fread("inst/extdata/naf_label.csv", data.table = FALSE,
    colClasses = c("integer", "character", "character"))
Encoding(naf_label[["label.fr"]]) = "UTF-8"
usethis::use_data(naf_label, overwrite = TRUE)

naf_esane = data.table::fread("inst/extdata/naf_esane.csv", data.table = FALSE, colClasses = "character")
naf_esane[11:27] = lapply(naf_esane[11:27], gsub, pattern = ",", replacement = ".")
naf_esane[11:27] = lapply(naf_esane[11:27], as.numeric)
Encoding(naf_esane[["type"]]) = "UTF-8"
usethis::use_data(naf_esane, overwrite = TRUE)

nomenclature_rpg_1 =
    data.table::fread("inst/extdata/nomenclature_rpg_1.csv", data.table = FALSE, colClasses = "character")
rownames(nomenclature_rpg_1) = scheme_rpg_1[["group"]]
Encoding(nomenclature_rpg_1[["group.label.fr"]]) = "UTF-8"
usethis::use_data(nomenclature_rpg_1, overwrite = TRUE)

nomenclature_rpg_2 =
    data.table::fread("inst/extdata/nomenclature_rpg_2.csv", data.table = FALSE, colClasses = "character")
rownames(nomenclature_rpg_2) = scheme_rpg_1[["culture"]]
Encoding(nomenclature_rpg_2[["culture.label.fr"]]) = "UTF-8"
usethis::use_data(nomenclature_rpg_2, overwrite = TRUE)

scheme_admin_express = 
    data.table::fread("inst/extdata/scheme_admin_express.csv", data.table = FALSE, colClasses = "character")
Encoding(scheme_admin_express[["label.fr"]]) = "UTF-8"
Encoding(scheme_admin_express[["comment"]]) = "UTF-8"
usethis::use_data(scheme_admin_express, overwrite = TRUE)

scheme_bd_topo_3 = 
    data.table::fread("inst/extdata/scheme_bd_topo_3.csv", data.table = FALSE, colClasses = "character")
Encoding(scheme_bd_topo_3[["label.fr"]]) = "UTF-8"
Encoding(scheme_bd_topo_3[["comment"]]) = "UTF-8"
usethis::use_data(scheme_bd_topo_3, overwrite = TRUE)

scheme_rpg_1 = data.table::fread("inst/extdata/scheme_rpg_1.csv", data.table = FALSE, colClasses = "character")
rownames(scheme_rpg_1) = scheme_rpg_1[["name"]]
Encoding(scheme_rpg_1[["label.fr"]]) = "UTF-8"
Encoding(scheme_rpg_1[["comment"]]) = "UTF-8"
usethis::use_data(scheme_rpg_1, overwrite = TRUE)

scheme_rpg_2 = data.table::fread("inst/extdata/scheme_rpg_2.csv", data.table = FALSE, colClasses = "character")
rownames(scheme_rpg_2) = scheme_rpg_2[["name"]]
Encoding(scheme_rpg_2[["label.fr"]]) = "UTF-8"
Encoding(scheme_rpg_2[["comment"]]) = "UTF-8"
usethis::use_data(scheme_rpg_2, overwrite = TRUE)

scheme_sirene_2019 = data.table::fread("inst/extdata/scheme_sirene_2019.csv", data.table = FALSE)
Encoding(scheme_sirene_2019[["label.french"]]) = "UTF-8"
Encoding(scheme_sirene_2019[["comment"]]) = "UTF-8"
usethis::use_data(scheme_sirene_2019, overwrite = TRUE)

scheme_sirene_na = data.table::fread("inst/extdata/scheme_sirene_na.csv", data.table = FALSE)
usethis::use_data(scheme_sirene_na, overwrite = TRUE)

bd_topo_nature_reclass = 
    data.table::fread("inst/extdata/bd_topo_nature_reclass.csv", data.table = FALSE, 
    colClasses = "character", encoding = "UTF-8", stringsAsFactors = TRUE)
usethis::use_data(bd_topo_nature_reclass, overwrite = TRUE)

bd_topo_wall = data.table::fread("inst/extdata/bd_topo_wall.csv", data.table = FALSE, 
        colClasses = "character", encoding = "UTF-8", stringsAsFactors = TRUE)
usethis::use_data(bd_topo_wall, overwrite = TRUE)

bd_topo_roof = data.table::fread("inst/extdata/bd_topo_roof.csv", data.table = FALSE, 
        colClasses = "character", encoding = "UTF-8", stringsAsFactors = TRUE)
usethis::use_data(bd_topo_roof, overwrite = TRUE)

scheme_insee_logement_2019 = data.table::fread("inst/extdata/scheme_insee_logement_2019.csv", data.table = FALSE, 
        colClasses = "character", encoding = "UTF-8", stringsAsFactors = FALSE)
scheme_insee_logement_2019[["keep"]] = as.logical(scheme_insee_logement_2019[["keep"]])
usethis::use_data(scheme_insee_logement_2019, overwrite = TRUE)

setwd("..")
