#!/usr/bin/env Rscript

library(sf)

### Usage sur floodam
##
##  nohup ./script_test_rpg_eaip.R > test_rpg_eaip.out &
##

# cat("Read eaip\n")
# eaip =  readRDS("~/data3/test-eaip-rpg/eaip.rds")

# cat("Read rpg\n")
# rpg = readRDS("~/data3/test-eaip-rpg/rpg.rds")


# initial = Sys.time()
# cat(sprintf("Perform st_intersection rpg - eaip. Start time: %s\n", initial))
# temp = sf::st_intersection(rpg, eaip)
# elapsed = fmt(Sys.time() - initial)
# cat(sprintf("... done. Time elpased %s\n", elapsed))
# saveRDS(temp, "~/data3/test-eaip-rpg/rpg_eaip_intersection.rds")

# initial = Sys.time()
# cat(sprintf("Perform st_intersects rpg - eaip. Start time: %s\n", initial))
# temp = sf::st_intersects(rpg, eaip)
# elapsed = fmt(Sys.time() - initial)
# cat(sprintf("... done. Time elpased %s\n", elapsed))
# saveRDS(temp, "~/data3/test-eaip-rpg/rpg_eaip_intersects.rds")

# initial = Sys.time()
# cat(sprintf("Perform st_intersection eaip - rpg. Start time: %s\n", initial))
# temp = sf::st_intersection(eaip, rpg)
# elapsed = fmt(Sys.time() - initial)
# cat(sprintf("... done. Time elpased %s\n", elapsed))
# saveRDS(temp, "~/data3/test-eaip-rpg/eaip_rpg_intersection.rds")

# initial = Sys.time()
# cat(sprintf("Perform st_intersects eaip - rpg. Start time: %s\n", initial))
# temp = sf::st_intersects(eaip, rpg)
# elapsed = fmt(Sys.time() - initial)
# cat(sprintf("... done. Time elpased %s\n", elapsed))
# saveRDS(temp, "~/data3/test-eaip-rpg/eaip_rpg_intersects.rds")

### Ne marche pas sur floodam !

library(sf)
library(lwgeom)
library(geojsonsf)

math = geojson_sf("croisement_rpg_eaip.geojson")
st_crs(math) = 2154
math = st_transform(math, 4326)
rpg = readRDS("rpg.rds")
eaip = readRDS("eaip.rds")
eaip_union = st_union(eaip)
eaip_bis = st_collection_extract(lwgeom::st_subdivide(eaip, 100), "POLYGON")

commune = readRDS("commune_D034_2020-06-29.rds")

eaip_0 = sf::st_intersection(eaip, mauguio)
eaip_1 = sf::st_intersection(eaip_bis, mauguio)
rpg_0 = sf::st_intersection(rpg, mauguio)

r_e_0 = sf::st_intersection(rpg_0, eaip_0)
e_r_0 = sf::st_intersection(eaip_0, rpg_0)
r_e_1 = sf::st_intersection(rpg_0, eaip_1)
e_r_1 = sf::st_intersection(eaip_1, rpg_0)

sum(st_area(r_e_0))
sum(st_area(r_e_1))
sum(st_area(e_r_0))
sum(st_area(e_r_1))

suppressMessages(suppressWarnings(microbenchmark::microbenchmark(
    r_e_0 = sf::st_intersection(rpg_0, eaip_0),
    e_r_0 = sf::st_intersection(eaip_0, rpg_0),
    r_e_1 = sf::st_intersection(rpg_0, eaip_1),
    e_r_1 = sf::st_intersection(eaip_1, rpg_0),
    times = 5
)))

so_ii = st_union(commune[commune[["commune"]] %in% floodam.data::commune_so_ii[["commune"]], ])

eaip_2 = sf::st_intersection(eaip_bis, so_ii)
rpg_2 = sf::st_intersection(rpg, so_ii)

r_e_2 = sf::st_intersection(rpg_2, eaip_2)
e_r_2 = sf::st_intersection(eaip_2, rpg_2)

sum(st_area(e_r_2))
sum(st_area(e_r_2))

suppressMessages(suppressWarnings(microbenchmark::microbenchmark(
    r_e_2 = sf::st_intersection(rpg_2, eaip_2),
    e_r_2 = sf::st_intersection(eaip_2, rpg_2),
    times = 5
)))


r_e_1 = sf::st_intersection(rpg, eaip_bis)
e_r_1 = sf::st_intersection(eaip_bis, rpg)
r_e_2 = sf::st_intersection(rpg[sapply(sf::st_intersects(rpg, eaip_bis), any), ], eaip_bis)
e_r_2 = sf::st_intersection(eaip_bis[sapply(sf::st_intersects(eaip_bis, rpg), any), ], rpg)

sum(st_area(r_e_1))
sum(st_area(e_r_1))
sum(st_area(r_e_2))
sum(st_area(e_r_2))

suppressMessages(suppressWarnings(microbenchmark::microbenchmark(
    r_e_1 = sf::st_intersection(rpg, eaip_bis),
    e_r_1 = sf::st_intersection(eaip_bis, rpg),
    r_e_2 = sf::st_intersection(rpg[sapply(sf::st_intersects(rpg, eaip_bis), any), ], eaip_bis),
    e_r_2 = sf::st_intersection(eaip_bis[sapply(sf::st_intersects(eaip_bis, rpg), any), ], rpg),
    times = 5
)))

# Unit: seconds
#   expr      min       lq     mean   median       uq      max neval
#  r_e_1 20.95350 21.08689 21.54870 21.46526 21.60983 22.62799     5
#  e_r_1 21.32801 21.86785 24.06424 23.47800 26.44594 27.20139     5
#  r_e_2 22.56394 22.59142 24.50463 22.76101 25.91764 28.68915     5
#  e_r_2 24.28175 25.11351 27.71212 26.04316 30.04670 33.07547     5
# Pas d'avantages (ni d'inconvénient flagrant) à faire un st_intersects avant st_intersection

# Test des corrections de géométries de l'EAIP

library(sf)
library(lwgeom)
library(geojsonsf)

rpg_base = readRDS("rpg.rds")
eaip_base = readRDS("eaip.rds")

resolution = c(50, 100, 150, 200)

subdivide_as_polygon = function(x, max_vertices) {
    suppressMessages(suppressWarnings(sf::st_collection_extract(lwgeom::st_subdivide(x, max_vertices), "POLYGON")))
}

eaip = lapply(resolution, subdivide_as_polygon, x = eaip_base)
names(eaip) = resolution

# microbenchmark::microbenchmark(
#     eaip_020 = subdivide_as_polygon(eaip_base, 20),
#     eaip_040 = subdivide_as_polygon(eaip_base, 40),
#     eaip_060 = subdivide_as_polygon(eaip_base, 60),
#     eaip_080 = subdivide_as_polygon(eaip_base, 80),
#     eaip_100 = subdivide_as_polygon(eaip_base, 100),
#     times = 5
# )
#
# mop4269
# Unit: seconds
#     expr      min       lq     mean   median       uq      max neval
# eaip_020 46.42311 48.67671 49.90333 49.86000 50.51321 54.04360     5
# eaip_040 37.18360 41.68838 44.12189 42.86486 45.62660 53.24599     5
# eaip_060 34.49033 39.78951 40.30719 40.62007 43.06508 43.57094     5
# eaip_080 33.00071 36.22247 37.66085 38.01649 40.15905 40.90552     5
# eaip_100 31.57992 32.24335 32.55857 32.38539 33.04991 33.53430     5
#
# floodam@cin-mo-floodam
# Unit: seconds
#      expr      min       lq     mean   median       uq      max neval   cld
#  eaip_020 55.21032 55.33594 55.53091 55.41421 55.80912 55.88496     5     e
#  eaip_040 44.73260 44.75717 44.93216 44.79303 45.15872 45.21928     5    d 
#  eaip_060 40.95725 41.21488 41.31441 41.35066 41.45254 41.59669     5   c  
#  eaip_080 39.02001 39.04303 39.44978 39.20630 39.83142 40.14814     5  b   
#  eaip_100 37.52291 37.64748 37.83604 37.68068 37.93524 38.39392     5 a
#
# microbenchmark::microbenchmark(
#     eaip_050 = subdivide_as_polygon(eaip_base, 50),
#     eaip_100 = subdivide_as_polygon(eaip_base, 100),
#     eaip_150 = subdivide_as_polygon(eaip_base, 150),
#     eaip_200 = subdivide_as_polygon(eaip_base, 200),
#     times = 1
# )
# floodam@cin-mo-floodam
# Unit: seconds
#      expr      min       lq     mean   median       uq      max neval
#  eaip_050 44.17756 44.17756 44.17756 44.17756 44.17756 44.17756     1
#  eaip_100 38.82300 38.82300 38.82300 38.82300 38.82300 38.82300     1
#  eaip_150 36.64707 36.64707 36.64707 36.64707 36.64707 36.64707     1
#  eaip_200 35.74502 35.74502 35.74502 35.74502 35.74502 35.74502     1

correct_sf = function(x, check = FALSE, verbose = TRUE) {
    if (verbose == TRUE) area = c("original" = round(sum(sf::st_area(x))))

    # First step, removal of polygons entirely included in other
    selection = unique(unlist(suppressMessages(suppressWarnings(sf::st_contains_properly(x)))))
    x = x[-selection, ]
    if (verbose == TRUE) area["included"] = round(sum(sf::st_area(x)))

    # Second step, union of overlapping polygons
    overlap = suppressMessages(suppressWarnings(sf::st_overlaps(x)))
    full = which(sapply(overlap, any))
    core = list()
    for (i in full) {
        if (all(unlist(core) != i)) core = c(core, list(c(i, overlap[[i]])))
    }
    result = x[sapply(core, "[", 1), ]
    for (i in nrow(result)) {
        sf::st_geometry(result[i,]) = sf::st_geometry(sf::st_union(x[core[[i]], ]))
    }
    result = rbind(x[-full, ], result)
    if (verbose == TRUE) area["overlap"] = round(sum(sf::st_area(result)))

    if (check == TRUE) {
        if(any(sapply(suppressMessages(suppressWarnings(sf::st_overlaps(result))), any)))
            cat("\tBad luck. There is still some overlapping!\n")
        if(any(sapply(suppressMessages(suppressWarnings(sf::st_contains_properly(result))), any)))
            cat("\tBad luck. There are still some polygons included in others!\n")
    }

    if (verbose == TRUE) {
        statistic = data.frame(
            value = area,
            absolute = c(0, diff(area)),
            relative = round(c(0, diff(area)/area["original"]), 5)
        )
        msg = capture.output(statistic)
        msg[1] = paste("", msg[1])
        cat(paste(msg, "\n"))
    }
    return(result)
}

# microbenchmark::microbenchmark(
#     c020 = correct_sf(eaip[["20"]], verbose = FALSE),
#     c040 = correct_sf(eaip[["40"]], verbose = FALSE),
#     c060 = correct_sf(eaip[["60"]], verbose = FALSE),
#     c080 = correct_sf(eaip[["80"]], verbose = FALSE),
#     c100 = correct_sf(eaip[["100"]], verbose = FALSE),
#     times = 5
# )
# 
# floodam@cin-mo-floodam
# Unit: seconds
#  expr      min       lq     mean   median       uq      max neval
#  c020 22.89567 22.89567 22.89567 22.89567 22.89567 22.89567     1
#  c040 15.30100 15.30100 15.30100 15.30100 15.30100 15.30100     1
#  c060 13.48588 13.48588 13.48588 13.48588 13.48588 13.48588     1
#  c080 12.60811 12.60811 12.60811 12.60811 12.60811 12.60811     1
#  c100 12.81117 12.81117 12.81117 12.81117 12.81117 12.81117     1
#
# microbenchmark::microbenchmark(
#     c050 = correct_sf(eaip[["50"]], verbose = FALSE),
#     c100 = correct_sf(eaip[["100"]], verbose = FALSE),
#     c150 = correct_sf(eaip[["150"]], verbose = FALSE),
#     c200 = correct_sf(eaip[["200"]], verbose = FALSE),
#     times = 5
# )
# floodam@cin-mo-floodam
# Unit: seconds
#  expr      min       lq     mean   median       uq      max neval cld
#  c050 14.04425 14.12735 14.20378 14.19680 14.29874 14.35176     5   b
#  c100 12.21147 12.22759 12.30018 12.23247 12.26930 12.56007     5  a 
#  c150 12.01485 12.19388 12.24984 12.20330 12.23674 12.60042     5  a 
#  c200 12.35412 12.37702 12.41459 12.38753 12.39271 12.56154     5  a 

eaip_corrected = lapply(eaip, correct_sf)

intersect_area = function(x, y, what = "area_int") {
    intersection = suppressMessages(suppressWarnings(sf::st_intersection(x, y)))
    x[["area"]] = sf::st_area(x)
    intersection[["area"]] = sf::st_area(intersection)
    value = aggregate(sf::st_drop_geometry(intersection["area"]), sf::st_drop_geometry(intersection["id"]), sum)
    value[[what]] = units::set_units(as.vector(value[["area"]]), "m*m")
    value = merge(sf::st_drop_geometry(x[c("id", "area")]), value[c("id", what)], all.x = TRUE)
    value[[what]] = value[[what]] / value[["area"]]
    value[[what]][is.na(value[[what]])] = 0
    return(merge(x, value))
}

# microbenchmark::microbenchmark(
#     rpg_020 = intersect_area(rpg_base, eaip_corrected[["20"]]),
#     rpg_040 = intersect_area(rpg_base, eaip_corrected[["40"]]),
#     rpg_060 = intersect_area(rpg_base, eaip_corrected[["60"]]),
#     rpg_080 = intersect_area(rpg_base, eaip_corrected[["80"]]),
#     rpg_100 = intersect_area(rpg_base, eaip_corrected[["100"]]),
#     times = 5
# )
# floodam@cin-mo-floodam
# Unit: seconds
#     expr      min       lq     mean   median       uq      max neval cld
#  rpg_020 46.35053 46.82791 46.88493 47.01081 47.08244 47.15299     5   c
#  rpg_040 40.14147 40.27441 40.87638 40.65648 41.60520 41.70434     5 a  
#  rpg_060 40.49828 40.60494 41.07091 40.86126 41.61340 41.77665     5 a  
#  rpg_080 39.78483 40.69373 41.13944 40.90605 41.65839 42.65422     5 a  
#  rpg_100 42.21448 42.53262 43.19570 43.64084 43.79498 43.79556     5  b 
#
# microbenchmark::microbenchmark(
#     rpg_050 = intersect_area(rpg_base, eaip_corrected[["50"]]),
#     rpg_100 = intersect_area(rpg_base, eaip_corrected[["100"]]),
#     rpg_150 = intersect_area(rpg_base, eaip_corrected[["150"]]),
#     rpg_200 = intersect_area(rpg_base, eaip_corrected[["200"]]),
#     times = 1
# )
# floodam@cin-mo-floodam
# Unit: seconds
#     expr      min       lq     mean   median       uq      max neval
#  rpg_050 40.15731 40.15731 40.15731 40.15731 40.15731 40.15731     1
#  rpg_100 41.72322 41.72322 41.72322 41.72322 41.72322 41.72322     1
#  rpg_150 46.05453 46.05453 46.05453 46.05453 46.05453 46.05453     1
#  rpg_200 51.02371 51.02371 51.02371 51.02371 51.02371 51.02371     1

rpg_eaip = lapply(eaip_corrected, intersect_area, x = rpg_base)

analysis = sapply(rpg_eaip, function(x) {as.vector(summary(x[["area_int"]]))})
rownames(analysis) = names(summary(1))
colnames(analysis) = resolution
analysis = rbind(analysis, 
    n_1.0001 = sapply(rpg_eaip, function(x) {sum(as.vector(x[["area_int"]]) > 1.0001) / nrow(x)}),
    n_1.00001 = sapply(rpg_eaip, function(x) {sum(as.vector(x[["area_int"]]) > 1.00001) / nrow(x)})
)
analysis

rpg_trouble = sapply(rpg_eaip, function(x) {which.max(as.vector(x[["area_int"]]))})
eaip_trouble = mapply(
    function(x, y, z) {unlist(st_intersects(x[z,], y))},
    rpg_eaip,
    eaip_corrected,
    rpg_trouble,
    SIMPLIFY = FALSE
)

mapply(function(x, y) {st_intersects(x[y, ])}, eaip_corrected, eaip_trouble)
mapply(function(x, y) {st_overlaps(x[y, ])}, eaip_corrected, eaip_trouble)
mapply(function(x, y) {st_contains_properly(x[y, ])}, eaip_corrected, eaip_trouble)

selection = "50"
plot(eaip_corrected[[selection]][eaip_trouble[[selection]], 0], border = c("blue", "red"))
plot(rpg_eaip[[selection]][rpg_trouble[[selection]], 0], add = TRUE)
