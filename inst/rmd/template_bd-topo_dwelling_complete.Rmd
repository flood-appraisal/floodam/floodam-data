---
output: 
    bookdown::pdf_book:
        latex_engine: xelatex
        toc: false
        number_sections: false
        fig_caption: true
        extra_dependencies:
            - booktabs: null            # jolis tableaux
            - xcolor: null              # gestion des couleurs
            - float: null
fontsize: 10pt
geometry: margin=8mm
lang: fr
link-citations: true
header-includes: 
    - \pagenumbering{gobble}
---

```{r setup, include=FALSE}
# Chunk options
knitr::opts_chunk$set(
    warning = FALSE,
    message = FALSE,
    error = FALSE,
    progress = FALSE, 
    verbose = FALSE,
    echo = FALSE,
    fig.pos = "!H",
    fig.align = "center",
    out.width = "100%"
)
```

\centering\large

**Rapport logement - [`r tab[['report']][['department']]`]**

\normalsize

BD Topo millésime `r tab[['report']][['v_bdtopo']]`

\begin{minipage}[c]{0.45\textwidth}
\centering

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["checkups_tab1"]],
            format = "latex",
            format.args = list(
                big.mark = " ",
                decimal.mark = ","
            ),
            booktabs = TRUE,
            row.names = FALSE,
            align = "c",
            linesep = ""
        ),
        latex_options = "hold_position",
        position = "left"
    ),
    c("nature" = 1, "destination" = 2)
)
```

\vspace{20pt}

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["checkups_tab2"]],
            format = "latex",
            format.args = list(
                big.mark = " ",
                decimal.mark = ","
            ),
            booktabs = TRUE,
            row.names = FALSE,
            align = "c",
            linesep = ""
        ),
        latex_options = "hold_position",
        position = "left"
    ),
    c("destination vs. dwelling" = 2)
)
```

\end{minipage}
\hfill
\begin{minipage}[c]{0.45\textwidth}

```{r, out.width = "82%"}
knitr::include_graphics(tab[["report"]][["map_build"]])
```

\end{minipage}

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["report"]][["nb_dwelling_doc"]],
            format = "latex",
            align = "r",
            format.args = list(
                big.mark = " ",
                decimal.mark = ","
            ),
            booktabs = TRUE,
            linesep  = c(rep("",nrow(tab[["report"]][["nb_dwelling_doc"]])-2),"\\midrule"),
        ),
        latex_options = "hold_position",
        position = "center"
    ),
    c(" " = 1, "polygon" = 1, "logement" = 2, "destination" = 3, "dwelling" = 2, "dest. vs. dwel." = 2)
)
```

\par\noindent\rule{\textwidth}{3pt}

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["dwelling_type"]],
            col.names = gsub("nb_dwel.","",dimnames(tab[["dwelling_type"]])[[2]]),
            format = "latex",
            format.args = list(
                big.mark = " ",
                decimal.mark = ","
            ),
            booktabs = TRUE,
            linesep = c(rep("",nrow(tab[["dwelling_type"]])-2),"\\midrule"),
        ),
        latex_options = "hold_position",
        position = "center"
    ),
    c(" " = 2, "polygon" = 5, "logement" = 5)
)   
```

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["report"]][["dwelling_info_doc"]],
            format = "latex",
            align = "r",
            booktabs = TRUE,
            linesep = c(rep("",nrow(tab[["report"]][["dwelling_info_doc"]])-2),"\\midrule"),
        ),
        latex_options = "hold_position",
        position = "center"
    ),
    c(" " = 2, "Pourcentage (%)" = 10)
)
```

```{r}
knitr::include_graphics(tab[["report"]][["plots"]][1])
```


```{r, fig.show="hold", out.width="49%"}
knitr::include_graphics(tab[["report"]][["plots"]][2:3])
```

```{r, fig.show="hold", out.width="49%"}
knitr::include_graphics(tab[["report"]][["map_dwel"]])
knitr::include_graphics(tab[["report"]][["map_dwel-nc"]])
```
\vspace{10pt}
\begin{minipage}[t]{0.52\textwidth}
\centering

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["report"]][["wall_roof_doc"]],
            format = "latex",
            align = "lrlr",
            format.args = list(
                big.mark = " ",
                decimal.mark = ","
            ),
            booktabs = TRUE,
            linesep = c(rep("",nrow(tab[["report"]][["wall_roof_doc"]])-2),"\\midrule"),
            row.names = FALSE
        ),
        latex_options = "hold_position",
        position = "left"
    ),
    c("mur" = 2, "toiture" = 2)
)
```

\end{minipage}
\hspace{.03\textwidth}
\begin{minipage}[t]{0.4\textwidth}
\centering

```{r}
kableExtra::add_header_above(
    kableExtra::kable_styling(
        knitr::kable(
            tab[["report"]][["wallroof_comb_doc"]],
            format = "latex",
            align = "r",
            format.args = list(
                big.mark = " ",
                decimal.mark = ","
            ),
            booktabs = TRUE,
            linesep = "",
            row.names = TRUE
        ),
        latex_options = "hold_position",
        position = "left"
    ),
    c(" " = 1, "matériaux combinés (%)" = 3)
)
```

\end{minipage}

\newpage

```{r}
tab[["complete"]] = dir(
    file.path(
        dirname(tab[["report"]][["map_dwel"]]),
        "synthesis",
        "graph"
    ),
    tab[["report"]][["department"]],
    full.names = TRUE
)
names(tab[["complete"]]) = gsub(
    sprintf(".*%s_", tab[["report"]][["department"]]),
    "",
    tools::file_path_sans_ext(tab[["complete"]])
)
```

\newpage

**Niveau (complet)**

**_SFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_level_SFH"])
```

**_TH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_level_TH"])
```

**_MFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_level_MFH"])
```

**_AB_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_level_AB"])
```

\newpage

**Niveau (EAIP)**

**_SFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_level_SFH"])
```

**_TH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_level_TH"])
```

**_MFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_level_MFH"])
```

**_AB_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_level_AB"])
```

\newpage

**Mur (complet)**

**_SFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_wall_SFH"])
```

**_TH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_wall_TH"])
```

**_MFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_wall_MFH"])
```

**_AB_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_wall_AB"])
```

\newpage

**Mur (EAIP)**

**_SFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_wall_SFH"])
```

**_TH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_wall_TH"])
```

**_MFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_wall_MFH"])
```

**_AB_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_wall_AB"])
```

\newpage

**Toit (complet)**

**_SFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_roof_SFH"])
```

**_TH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_roof_TH"])
```

**_MFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_roof_MFH"])
```

**_AB_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["all_roof_AB"])
```

\newpage

**Toit (EAIP)**

**_SFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_roof_SFH"])
```

**_TH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_roof_TH"])
```

**_MFH_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_roof_MFH"])
```

**_AB_**

```{r, out.width = "82%"}
knitr::include_graphics(tab[["complete"]]["eaip_roof_AB"])
```
