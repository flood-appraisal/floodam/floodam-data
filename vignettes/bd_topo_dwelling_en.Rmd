---
title: "Dwelling from BD Topo (building)"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Dwelling from BD Topo (building)}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(floodam.data)
```

# Steps

1. paths
1. extract_building
1. extract_dwelling
1. analyse_dwelling
1. generate_report
1. report