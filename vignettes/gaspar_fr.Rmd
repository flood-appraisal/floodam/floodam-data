---
title: "gaspar (French)"
author: "Frédéric Grelot"
date: "2022-02-27"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{gaspar (French)}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  \usepackage[utf8]{inputenc}
---

```{r}
#| include: false
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# Description de la base de données gaspar

La base de donnée **gaspar** (acronyme de gestion assistée des procédures administratives relatives aux risques naturels) contient les informations relatives aux procédures administratives de la gestion des risques par l'État français. Sous la responsabilité de la direction générale de la prévention des risques (DGPR), elle est mise à jour par les services départementaux du ministère en charge de l'environnement. Elle est disponible à l'adresse suivante : https://www.georisques.gouv.fr/donnees/bases-de-donnees/base-gaspar. Sur cette page sont également accessibles des éléments de description de la nature des données, qui ont servi à la rédaction de cette vignette.

La périodicité de la mise à jour de **gaspar** n'est pas indiquée, **gaspar** est réputé être mise à jour dès que de nouvelles informations sont disponibles.

Les informations disponibles dans **gaspar** sont organisées sous forme de fichiers thématiques :

- azi_gaspar.csv : documents d'information préventive de type atlas des zones inondables (AZI)
- catnat_gaspar.csv : procédures de type reconnaissance de l'état de catastrophes naturelles, actualisées dans les 30 jours après leur parution au Journal Officiel, maintenu par la Caisse Centrale de Réassurance (CCR)
- dicrim_gaspar.csv : documents d'information préventive de type document d'information Communal des populations sur les risques majeurs (DICRIM) ;
- pcs_gaspar.csv : documents d'information préventive de type plan communal de sauvegarde (PCS)
- pprm_gaspar.csv : état d'avancées des plans de prévention des risques miniers (PPRM)
- pprn_gaspar.csv : état d'avancées des plans de prévention des risques naturels (PPRN)
- pprt_gaspar.csv : état d'avancées des plans de prévention des risques technologiques (PPRN)
- risq_gaspar.csv: recensement des risques des communes conformes aux dossiers départementaux sur les risques majeurs (DDRM))
- tim_gaspar.csv : documents d'information préventive de type dossier de transmission d'information au maire (TIM)

Le format de données est de type csv, séparateur ";".

## Thème "catnat"

Le thème "catnat" contient les informations sur les procédures de reconnaissance de l'état de catastrophes naurelles. Chaque ligne correspond à une reconnaissance pour une commune et pour un événement (au sens d'un type de risque, d'une date de début et une date de fin). L'ensemble des champs disponibles dans le thème sont donnés dans le tableau suivant.

```{r}
#| label: "catnat-origin"
#| echo: false
knitr::kable(
    floodam.data::scheme_gaspar_catnat[c(2, 6)],
    row.names = FALSE,
    caption = "Variables disponibles dans le thème catnat")
```

Le type de risque ("num_risque_jo" dans la nomemclature d'oririgine et 'catnat' dans la nomenclature après traitement) est codifié selon les deux premières colonnes du tableau \@ref(tab:catnat-classification). La variable 'catnat' correspond à la codification du type de risque utilisé lors de la publication dans le journal officiel.


```{r}
#| label: "catnat-classification"
#| echo: false
DT::datatable(
    floodam.data::catnat_classification,
    rownames = FALSE,
    caption = "Classification des événements pour le thème catnat",
    options = list(dom = 'tip')
)
```

## Thème "pprn"

Le thème "pprn" contient les informations sur les procédures en lien avec la mise en place des plans de prévention des risques naturels. Chaque ligne correspondant à une procédure pour une commune et pour un type de risque. A priori, la base contient un historique complet des procédures, que celles-ci soient en vigueur ou ait été abrogées.

```{r}
#| label: "pprn-origin"
#| echo: false
knitr::kable(
    floodam.data::scheme_gaspar_pprn[c(2, 6)],
    row.names = FALSE,
    caption = "Variables disponibles dans le thème catnat")
```

Le type de risque ("num_risque" dans la nomemclature d'oririgine et "hazard" dans la nomenclature après traitement) est codifié selon les deux premières colonnes du tableau \@ref(tab:pprn-classification).

```{r}
#| label: "pprn-classification"
#| echo: false
DT::datatable(
    floodam.data::pprn_classification,
    rownames = FALSE,
    caption = "Classification des événements pour le thème pprn",
    options = list(dom = "tip")
)
```

# Manipulation de gaspar avec floodam.data

**floodam.data** permet de gérer la maintenance d'une base de donnée adaptée.

Les fonctions clés utilisées sont :

- download_gaspar pour télécharger la base de donnée distante.
- adapt_gaspar pour adapter la base de donnée en :
    - appliquant un schéma de données aux thèmes sélectionnés
    - sélectionnant les observations correspondant à des inondations
    - sélectionnant (de façon optionnelle) les observations pour les communes correspondant à la variable 'scope'
- alert_gaspar pour :
    - analyser si une version de gaspar adaptée est différente d'une autre
    - si tel est le cas, émettre une alerte sur un serveur Mattermost
    - supprimer la plus ancienne des versions

## Téléchargement de gaspar

Il y a 3 sources possibles pour le téléchargement de la base de donnée :

- https://files.georisques.fr/GASPAR/gaspar.zip
- https://data.cquest.org/georisques/gaspar.zip
- http://files.opendatarchives.fr/georisques.gouv.fr/gaspar.zip

Les deux dernières sources sont des miroirs de la première.

La fonction download_gapsar permet d'accéder à chacune de ces trois sources en utilisant le paramètre "origin".

```{r}
#| label: "download-gaspar"
#| eval: false
library(floodam.data)
destination = file.path("gaspar", "download")
download_gaspar(destination)
download_gaspar(destination, origin = "cquest")
download_gaspar(destination, origin = "opendatarchives")
```

Comme il n'y aucune indication de la version de la base de donnée téléchargée, le téléchargement se fait dans un sous-dossier du chemin indiqué dans le paramètre "destination" indiquant le jour du téléchargement. Les téléchargements du chunk précédent auraient par exemple été réalisés dans le dossier `r file.path("gaspar", "download", Sys.Date())`.

## Adaptation des données

### schéma appliqué aux données

Le schéma appliqué aux données dépend du thème choisi. Pour le thème "catnat" c'est celui de la variable globale `floodam.data::scheme_gaspar_catnat` dont le détail est donné dans le tableau suivant.

```{r}
#| label: "scheme-gaspar-catnat"
#| echo: false
knitr::kable(
    floodam.data::scheme_gaspar_catnat[c(2, 1, 4:5)],
    row.names = FALSE,
    caption = "Schéma appliqué pour le thème catnat")
```

### sélection des événements

Pour le thème "catnat", tous les événements ne sont pas conservés. Une sélection est réalisée pour conserver ceux qui s'apparentent à des phénomènes d'inondation. À partir d'une analyse des intitulés correspondants, disponibles dans la variable 'catnat_label_fr', une classification a été réalisée qui est disponible dans la variable globale `floodam.data::catnat_classification`, dont le détail est donné dans le tableau suivant. 

Cette classification est utilisée pour sélectionner les événements correspondant à une inondation de façon générale, c'est-à-dire pour lesquels au moins une des colonnes `r paste(names(floodam.data::catnat_classification)[-(1:2)], collapse = ", ")` est `TRUE`.

Les colonnes `r paste(names(floodam.data::catnat_classification)[-(1:2)], collapse = ", ")` sont ajoutées aux données 'catnat' pour permettre des analyses plus fines ultérieurement.

### sélection par le scope

Chaque observation étant associée à une commune par son code insee (variable 'commune'), une sélection du thème gaspar par une variable donnant l'étendue d'une zone d'étude est possible au moment de l'adaptation de la base.

## Quelques analyses à la volée

