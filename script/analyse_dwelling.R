package = "floodam.data" ; devtools::document(package) ; devtools::load_all(package)

# setting up paths 
path_building <- "data-local/floodam-data/adapted/bd-topo/building"
path_dwelling <- "data-local/floodam-data/adapted/bd-topo/dwelling"

# calling function. Long version 
tab = analyse_dwelling(
    origin = path_building,
    destination = path_dwelling,
    archive = "bdtopo-building-2022-03-15-D034.rds", 
    category = "Résidentiel",
    vintage = "2022-03-15"
)

# calling function. short version 
tab = analyse_dwelling(
    archive = "bdtopo-building-2022-03-15-D034.rds", 
    origin = path_building,
    destination = path_dwelling
)

# calling function. Recursive version with all default values
tab = analyse_dwelling(path_building)

# report
generate_report(
    origin = path_dwelling,
    destination = path_dwelling,
    archive = tab
)

# report. recursive
generate_report(file.path(path_dwelling, "analysed"))



