
package = "floodam.data"
devtools::load_all(package)

destination = tempdir()
origin = file.path(destination, "origin")
dir_data = file.path("to-adaptation", "datasets")

# Download and adapt last version of the BAN
v_ban = "2024-02-07"

download_ban(
    origin,
    department = 34,
    date = v_ban
)

ban = adapt_ban(
  origin = file.path(origin, "ban", v_ban),
  destination = file.path(destination, "adapted"),
  retrieve = TRUE
)

ban = select_scope(ban, so.ii::so_ii_scope)
n_ban = nrow(ban)

# Need to add commune_name to ban
# - use v_commune_2023 from INSEE ()
f_cog = file.path(dir_data, "v_commune_2023.csv.gz")
cog_2023 = data.table::fread(f_cog, data.table = FALSE)
cog_2023 = cog_2023[c("COM", "LIBELLE")]
names(cog_2023) =  c("commune", "commune_name")
cog_2023 = select_scope(cog_2023, so.ii::so_ii_scope)

# Pb avec Saint-Christol → on enlève
selection = cog_2023[["commune_name"]] != "Saint-Christol"
cog_2023 = cog_2023[selection, ]

ban = merge(ban, cog_2023, all.x = TRUE)
if (n_ban != nrow(ban)) {
    message("Pertes de données en ajoutant les noms des communes")
}

# Load last version of observation
v_observation = "2023-08-29"
f_observation = sprintf("observation_%s.csv.gz", v_observation)
observation = data.table::fread(file.path(dir_data, f_observation), data.table = FALSE)

# update ban

treatment = update_ban(
    observation,
    ban,
    v_observation,
    v_ban,
    id_ban_i = "ban_id",    
    id_ban_f = "id"
)

f_obs = sprintf("observation_%s.csv.gz", v_ban)
data.table::fwrite(treatment[["adapted"]], file.path(dir_data, f_obs))
f_cor = sprintf("observation_cor_%s.csv.gz", v_ban)
data.table::fwrite(treatment[["correction"]], file.path(dir_data, f_cor))

# Analyse des ban_version
table(treatment[["adapted"]][["ban_version"]])

# Début des analyses des corrections à effectuer
correction = treatment[["correction"]]

correction[["protection"]] = ifelse(
    correction[["n_protection"]] > 0,
    "with",
    "none"
)
analyse_correction = merge(
    cog_2023,
    aggregate(
        data.frame("survey" = correction[["protection"]]),
        by = correction[c("commune", "campaign", "protection", "survey_state")],
        length
    )
)
analyse_correction

# update_state

state = update_state(treatment[["adapted"]], ban)
f_state = sprintf("state_%s.rds", v_ban)
saveRDS(state, file.path(dir_data, f_state))

# Analyse 
table(state[["state"]])

# observations selon state

## tableau commune / state
state_df = sf::st_drop_geometry(state)
state_commune = merge(
    cog_2023,
    aggregate(
        data.frame(address = state_df[["ban_id"]]),
        by = state_df[c("state", "commune")],
        length,
        drop = FALSE
    )
)
head(state_commune)

prot_commune = merge(
    cog_2023,
    aggregate(
        state_df[c("n_protection", "n_building", "n_fence")],
        by = state_df[c("commune")],
        sum,
        na.rm = TRUE
    )
)
nmax_commune[nmax_commune[["n_protection"]] > -Inf,]
addmargins(table(state_df[c("commune", "state")]))

## carte correspondante
col = c("unobserved" = "gray", "none" = "green", "with" = "red")
cex = c("unobserved" = .5, "none" = .5, "with" = 1)
so.ii::map_so_ii(state[0], col = col[state[["state"]]], pch = 16, cex = cex[state[["state"]]], theme = "collectivity")

selection = unique(state_df[!is.na(state_df[["n_protection"]]), "commune"])
for (com in selection) {
    f_map = sprintf("state_%s_%s.png", com, v_ban)
    so.ii::map_so_ii(
        state[0],
        col = col[state[["state"]]], pch = 16, cex = cex[state[["state"]]],
        main = cog_2023[cog_2023[["commune"]] == com, "commune_name"],
        theme = "clc", scope = com,
        path = file.path(dir_data, "map", f_map)
    )
}
