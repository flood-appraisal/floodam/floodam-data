    # loading libraries
    library(sf); sf::sf_use_s2(FALSE)

    # setting up paths
    data_hydro = "data-local/hydraulique/AUDE-2022-11-07"
    path_orig = "data-local/floodam-data/original"
    path_adap = "data-local/floodam-data/adapted"
    output = "data-local/floodam-data/adapted/bd-topo/dwelling/event"
    path_fig = "data-local/gt-amc/figure/"
    path_tab = "data-common/gt-amc/table/bd_topo"

    # loading databases
    flood = sf::read_sf(file.path(data_hydro, "2018OCT15_AUDE_assemble_EZI_S.shp"))
    commune = readRDS(
        file.path(
            path_adap, 
            "admin-express",
            "commune_D011_2020-08-17.rds"
        )
    )
    department = readRDS(
        file.path(
            path_adap, 
            "admin-express",
            "departement_2020-08-17.rds"
        )
    )
    dwelling_geo = readRDS(
        file.path(
            path_adap, 
            "bd-topo/dwelling/event",
            "bdtopo-dwelling-2022-03-15-D011_inondation-2018.rds"
        )
    )
    insee = read.csv2(
        unzip(
            file.path(path_orig, "insee/BTT_TD_LOG1_2019.zip"),
            "BTT_TD_LOG1_2019.csv"
        )
    )[ ,c("CODGEO", "TYPLR", "NB")]
    names(insee) = c("commune", "type_log", "dwelling")

    # pre-processing databases
    dwelling = sf::st_drop_geometry(dwelling_geo)
    dwelling = droplevels(dwelling)
    dwelling[["typology"]][is.na(dwelling[["typology"]])] = "NC"
    dwelling[["typology"]] = factor(
        dwelling[["typology"]],
        levels = c("AB", "MFH", "SFH", "TH", "NC")
    )

    insee[["dwelling"]] = as.numeric(insee[["dwelling"]])
    insee[["typology"]] = cut(
        insee[["type_log"]],
        breaks = c(0:3),
        labels = c("SFH/TH","AB/MFH","NC")
    )
    insee[["typology"]] = factor(
        insee[["typology"]], 
        levels = c("AB/MFH", "SFH/TH", "NC")
    )

    # initializing output list
    result = list()

    # analysis: flooded buildings and total buildings by type of dwelling and commune
    temp = list()

    ## flooded buildings
    dataset = droplevels(dwelling[dwelling[["in_zi"]] == TRUE, ])
    temp[["flooded"]] = addmargins(
        table(dataset[["typology"]]), 
        FUN = list(total = sum)
    )

    ## all buildings
    dataset = dwelling
    temp[["all"]] = addmargins(
        table(dataset[["typology"]]), 
        FUN = list(total = sum)
    )

    ## flooded over total
    temp[["perc_flooded"]] = round((temp[["flooded"]]/temp[["all"]])*100, digits=1)
    temp[["perc_flooded"]][is.na(temp[["perc_flooded"]])] = 0

    ## comparative output
    temp = Reduce(rbind, temp)
    rownames(temp) = c("inondé", "total", "perc_inondé")
    temp = data.frame(
        typology=rownames(t(temp)),
        t(temp)
    )

    # storing results
    result[["building"]] = temp

    # analysis: flooded dwelling and total dwelling by type of dwelling and commune
    temp = list()

    ## flooded → need to talk w/ Fred about it 
    dataset = dwelling[dwelling[["in_zi"]] == TRUE, ]
    temp[["flooded"]] = setNames(aggregate(
            x = dataset[["dwelling"]], 
            by = list(dataset[["typology"]]),
            sum,
            drop = FALSE
        ), c("typology", "inondé"))
    temp[["flooded"]] = rbind(temp[["flooded"]], data.frame(typology='total', inondé = sum(temp[["flooded"]][, -1], na.rm = TRUE)))

    ## all buildings
    dataset = dwelling
    temp[["all"]] = setNames(aggregate(
            x = dataset[["dwelling"]], 
            by = list(dataset[["typology"]]),
            sum,
            drop = FALSE
        ), c("typology", "all"))
    temp[["all"]] = rbind(temp[["all"]], data.frame(typology='total', all = sum(temp[["all"]][, -1], na.rm = TRUE)))

    ## summary
    temp[["comparative"]] = merge(temp[["flooded"]], temp[["all"]], by = "typology", sort = FALSE)
    temp[["comparative"]][["perc_flooded"]] = round(
        (temp[["comparative"]][["inondé"]]/
        temp[["comparative"]][["all"]])*100, 
        digits = 1
    )

    ## insee data
    selection = insee[["commune"]] %in% dwelling[["commune"]]
    dataset = droplevels(insee[selection, c("commune", "dwelling", "typology")])
    temp[["insee"]] = setNames(aggregate(
            x = dataset[["dwelling"]], 
            by = list(dataset[["typology"]]),
            function(x) ceiling(sum(x)),
            drop = FALSE
        ), c("typology", "insee"))

    temp[["insee"]][["bd_topo"]] = c(
        sum(temp$all$all[1:2]), 
        sum(temp$all$all[3:4]), 
        temp$all$all[5]
    )
    temp[["insee"]] = rbind(temp[["insee"]], data.frame(typology='total', t(colSums(temp[["insee"]][, -1], na.rm = TRUE))))

    # storing results
    result[["dwelling"]] = temp[["comparative"]]
    result[["insee"]] = temp[["insee"]]

    # analysis: flooded dwellings by subtype of dwelling
    temp = list()

    ### → fait très vite. A refaire plus proprement en utilisant data.frame subtypes
    dwelling[["subtype"]] = "NC"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="SFH" &
        dwelling[["level"]] == 1
    ] = "SFH_l1"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="SFH" &
        dwelling[["level"]] == 2
    ] = "SFH_l2"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="SFH" &
        dwelling[["level"]] > 2
    ] = "SFH_>l2"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="TH" &
        dwelling[["level"]] == 1
    ] = "TH_l1"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="TH" &
        dwelling[["level"]] %in% c(2,3)
    ] = "TH_l2-3"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="TH" &
        dwelling[["level"]] > 3
    ] = "TH_>l3"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="MFH" &
        dwelling[["level"]] < 2
    ] = "MFH_<l2"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="MFH" &
        dwelling[["level"]] %in% c(2,3)
    ] = "MFH_l2-3"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="MFH" &
        dwelling[["level"]] %in% c(4,5)
    ] = "MFH_l4-5"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="MFH" &
        dwelling[["level"]] > 5
    ] = "MFH_>l5"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="AB" &
        dwelling[["level"]] < 2
    ] = "AB_<l2"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="AB" &
        dwelling[["level"]] %in% c(2,3)
    ] = "AB_l2-3"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="AB" &
        dwelling[["level"]] %in% c(4:10)
    ] = "AB_l4-10"
    dwelling[["subtype"]][
        dwelling[["typology"]] =="AB" &
        dwelling[["level"]] > 10
    ] = "AB_>l10"

    dwelling[["subtype"]] = factor(
        dwelling[["subtype"]],
        levels = c(
            "AB_<l2",
            "AB_l2-3",
            "AB_l4-10",
            "AB_>l10",
            "MFH_<l2",
            "MFH_l2-3",
            "MFH_l4-5",
            "MFH_>l5",
            "SFH_l1",
            "SFH_l2",
            "SFH_>l2",
            "TH_l1",
            "TH_l2-3",
            "TH_>l3",
            "NC"
            )
        )

    dataset =dwelling[dwelling[["in_zi"]] == TRUE, ]

    temp1 = setNames(aggregate(
        x = dataset[["dwelling"]], 
        by = list(dataset[["subtype"]]),
        function(x) length(x),
        drop = FALSE
    ), c("typology", "batiment"))


    temp2 = setNames(aggregate(
        x = dataset[["dwelling"]], 
        by = list(dataset[["subtype"]]),
        function(x) sum(x),
        drop = FALSE
    ), c("typology", "logement"))

    temp = merge(temp1,temp2, by = "typology", sort = FALSE)
    temp = temp[order(temp$typology),]
    temp[["niveau"]] = gsub("[A-Za-z_]","",as.character(temp[["typology"]]))
    temp[["typology"]] = gsub("[0-9l\\_\\<\\>\\-]","",as.character(temp[["typology"]]))
    temp = temp[c("typology", "niveau", "batiment", "logement")]
    temp = rbind(temp, data.frame(typology='total', niveau = "", t(colSums(temp[, c(3,4)], na.rm = TRUE))))

    # storing in results
    result[["subtype"]]=temp

    # analysis: flooded dwellings by construction date groups
    temp = list()

    date = c(0, 1947, 1967, 1974, 1981, 1989, 2000, 2023)
    dwelling[["time_period"]] = cut(
        as.numeric(format(dwelling[["date_apparition"]], "%Y")),
        breaks = date,
        labels = c(
            "<1947",
            "1947-1967",
            "1968-1974",
            "1975-1981",
            "1982-1989",
            "1990-2000",
            "2001>"
        )
    )
    dwelling[["time_period"]] = addNA(dwelling[["time_period"]])

    dataset = dwelling[dwelling[["in_zi"]] == TRUE, ]
    temp1 = setNames(aggregate(
        x = dataset[["dwelling"]], 
        by = list(dataset[["typology"]], dataset[["time_period"]]),
        function(x) length(x),
        drop = FALSE
    ), c("typology", "time_period", "batiment"))

    temp2 = setNames(aggregate(
        x = dataset[["dwelling"]], 
        by = list(dataset[["typology"]], dataset[["time_period"]]),
        function(x) sum(x),
        drop = FALSE
    ), c("typology", "time_period", "logement"))

    temp = merge(temp1,temp2, sort = FALSE)
    temp = temp[order(temp$typology),]
    temp[["time_period"]] = as.character(temp[["time_period"]])
    temp[["time_period"]][is.na(temp[["time_period"]])] = "NC"
    names(temp) = c("typology", "time_period", "batiment", "logement")
    temp = rbind(temp, data.frame(typology='total', time_period = "", t(colSums(temp[, c(3,4)], na.rm = TRUE))))

    # storing in results
    result[["time_period"]]=temp

    # analysis: crossed analysis of flooded dwellings by subtype of dwelling & 
    temp = list()

    # construction date groups
    dataset = dwelling[dwelling[["in_zi"]] == TRUE, ]

    temp1 = setNames(aggregate(
        x = dataset[["dwelling"]], 
        by = list(dataset[["subtype"]], dataset[["time_period"]]),
        function(x) length(x),
        drop = FALSE
    ), c("typology", "time_period", "batiment"))

    temp2 = setNames(aggregate(
        x = dataset[["dwelling"]], 
        by = list(dataset[["subtype"]], dataset[["time_period"]]),
        function(x) sum(x),
        drop = FALSE
    ), c("typology", "time_period", "logement"))

    temp = merge(temp1,temp2)
    temp = temp[order(temp$typology),]
    temp[["niveau"]] = gsub("[A-Za-z_]","",as.character(temp[["typology"]]))
    temp[["typology"]] = gsub("[0-9l\\_\\<\\>\\-]","",as.character(temp[["typology"]]))
    temp[["time_period"]] = as.character(temp[["time_period"]])
    temp[["time_period"]][is.na(temp[["time_period"]])] = "NC"
    temp = temp[c("typology","niveau", "time_period", "batiment", "logement")]
    temp = rbind(temp, data.frame(typology='total', niveau = "", time_period = "", t(colSums(temp[, c(4,5)], na.rm = TRUE))))


    # storing in results
    result[["time_subtype"]] = temp


    # storing tables
    files = c(
        'building'="aude-2018-typology-batiment.csv",
        'dwelling'="aude-2018-typology-logement.csv",
        'insee'="aude-2018-insee.csv",
        'subtype'="aude-2018-level.csv",
        'time_period'="aude-2018-date.csv",
        'time_subtype'="aude-2018-date-level.csv"
    )


    lapply(
        names(result),
        function (x,y) write.csv(result[x],file.path(path_tab, y[x]),row.names = FALSE),
        y = files
    )

# maps 
## checking crs
if(st_crs(flood) != st_crs(dwelling_geo)) {
    flood = sf::st_transform(flood, sf::st_crs(dwelling_geo))
}
if(st_crs(commune) != st_crs(dwelling_geo)) {
    commune = sf::st_transform(commune, sf::st_crs(dwelling_geo))
}
if(st_crs(department) != st_crs(dwelling_geo)) {
    department = sf::st_transform(department, sf::st_crs(dwelling_geo))
}

## colors
dwelling_geo[["color"]] = "grey60"
dwelling_geo[["color"]][dwelling[["in_zi"]] == TRUE] = "black"

{png(file.path(path_fig, "aude-inondation-2018.png"), width = 720, height = 720)
par(mar=rep(0.1, 4))
plot(commune$geometry, col = NA, border = "grey30", lwd = .4)
plot(
    department[department[["department"]] == "11","geometry"], 
    col = NA, 
    border = "black", 
    lwd = 1,
    add = TRUE
)
plot(
    commune[commune$commune %in% dwelling$commune, "geometry"], 
    col = "#b6b6b6", 
    border = "grey30", 
    lwd = .4, add = TRUE
)
plot(
    flood$geometry, 
    col = scales::alpha("dodgerblue", 0.6), 
    border = NA, 
    add = TRUE
)
dev.off()}

{png(file.path(path_fig, "aude-bati.png"), width = 720, height = 720)
par(mar=rep(0.1, 4))
plot(
    commune[commune$commune %in% dwelling$commune, "geometry"], 
    col = NA, 
    border = "grey30", 
    lwd = .6
)
plot(
    flood$geometry, 
    col = scales::alpha("dodgerblue", 0.6), 
    border = NA, 
    add = TRUE
)
plot(
    dwelling_geo["in_zi"], 
    col = dwelling_geo[["color"]], 
    border = dwelling_geo[["color"]],
    lwd = 0.7,
    add = TRUE
)
dev.off()}
