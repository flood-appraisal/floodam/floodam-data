test_that("Test interpret_args",
    {
        reponse = list(
            a = "1",
            b = "b=2",
            "3"
        )
        args = paste(names(unlist(reponse)), unlist(reponse), sep = "=")
        expect_identical(
            interpret_args(args),
            reponse
        )
    }
)
