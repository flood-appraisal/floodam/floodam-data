test_that("Test get_date_from_html",
    {
        x = c(
            "<html>",
            "<head><title>Index of /example/</title></head>",
            "<body>",
            "<h1>Index of /example/</h1><hr><pre>",
            "<a href='../'>../</a>",
            "<a href='parent/2023-11/archive.csv.gz'>archive.csv.gz</a>",
            "<a href='2023-12/archive.csv.gz'>archive.csv.gz</a>",
            "<a href=\"2024-01/\">2024-01/</a>              07-Jan-2024 09:54",
            "<a href=\"parent/child/2024-02/\">2024-02/</a> 07-Feb-2024 09:54",
            "<a href=\"2024-03\">2024-03</a>                07-Mar-2024 10:02",
            "<a href=\"2024-01-01/\">2024-01-01/</a>        02-Jan-2024 09:56",
            "<a href=\"2024-01-02\">2024-01-02</a>          03-jan-2024 11:02",
            "<a href=\"2023/\">2023/</a>                    01-Feb-2023 15:54",
            "<a href=\"2024\">2024</a>                      01-Feb-2024 15:54",
            "<a href=\"last/\">last/</a>                    07-Mar-2024 10:02",
            "</pre><hr></body>",
            "</html>"
        )

        month = c("2024-01", "2024-02", "2024-03")
        last = c(month, "last")
        day = c("2024-01-01", "2024-01-02")
        year = c("2023", "2024")
        combine = c(month, day, year)

        expect_identical(get_date_from_html(x = x), month)
        expect_identical(get_date_from_html(x = x, expected = "month"), month)
        expect_identical(get_date_from_html(x = x, expected = "day"), day)
        expect_identical(get_date_from_html(x = x, expected = "any"), combine)
        expect_identical(get_date_from_html(x = x, last = TRUE), last)
        expect_identical(get_date_from_html(x = x[-(8:10)]), character())
        last = expect_warning(get_date_from_html(x = x[-(8:10)], last = TRUE))
        expect_identical(last, "last")
    }
)