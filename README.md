# floodam.data <img src="man/figures/logo.png" align="right" width="25%" height="25%"/>
## Welcome to floodam.data

**floodam.data** is an R library for dowloading and preparing (spatial) database (DB) that may be used as input for cost-benefit analysis (CBA) applied to flood management policies.

data prepared by **floodam.data** are particularly suited to be used with **floodam.spatial**.

This library has been developed within the French working group "GT AMC" which aims at developing methodologies for the economic appraisal of flood management policies.
In this context, the development of **floodam.data** has received the support of the French Ministry in charge of the Environment.

## What **floodam.data** can do

**floodam.data** can download data from different sources:

- BD TOPO from IGN
- RPG from IGN or cquest
- geo_sirene from cquest

**floodam.data** can perform elementary analysis:

- analyse integrity of downloaded data
- enhance spatial DB with flood information from EAIP
- log how download and 

## What **floodam.data** cannot do

**floodam.data** cannot (and this won't be done without someone interested):

- download data that are not produced in a French context.

**floodam.data** cannot (but should at some point be able to) :

- download and prepare data from INSEE.

## How to get **floodam.data** ?

You can download and install it from this archive:
[www.floodam.org/library/floodam.data_0.9.43.0.tar.gz](http://www.floodam.org/library/floodam.data_0.9.43.0.tar.gz).

For instance, from a terminal, the command you can use is:

```{.bash filename="terminal"} 
R -e "install.packages(
    pkgs = 'http://www.floodam.org/library/floodam.data_0.9.43.0.tar.gz',
    type = 'source',
    repos = NULL,
    dependencies = TRUE)"
```

## How to help for the development of **floodam.data**

Contact us.
