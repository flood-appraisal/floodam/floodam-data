#' @title Download Admin Express archives
#'
#' @description
#' Function used to downlaod Admin Express archives. The function has preset
#' options for its use with IGN website and data.cquest.org.
#' 
#' @details
#' If \code{origin == "ign"}, \bold{origin} is set to
#' "https://geoservices.ign.fr/adminexpress"
#' 
#' If \code{origin == "cquest"}, \bold{origin} is set to
#' "https://data.cquest.org/ign/adminexpress"
#'
#'
#' @param destination character, the address where dara are stocked.
#' @param origin character, a keyword or the address from where data are
#' downloaded.
#' @param name character, vector of acceptable names fo archive to be
#' downloaded.
#' @param extension character, vector of acceptable types of archive to be
#' downloaded.
#' @param version string, version of Admin Express to be downloaded.
#' @param date character, date of the archive to be downloaded.
#' @param verbose logical, should the function send some messages while running.
#'
#' @return nothing
#'
#' @export
#'
#' @examples
#' \dontrun{
#' destination = tempdir()
#' download_admin_express(destination, version = "MYT")
#' unlink(destination)
#' }

download_admin_express = function(
    destination,
    origin = c("ign", "cquest"), 
    name = c("ADMIN-EXPRESS-COG", "ADMIN-EXPRESS", "ADMIN-EXPRESS-COG-CARTO"),
    extension = "7z",
    date = "last",
    version = NULL,
    verbose = TRUE
) {
    origin = match.arg(origin)
    name = match.arg(name)
    extension = match.arg(extension)

    if(origin == "ign") {
        origin = "https://geoservices.ign.fr/adminexpress"
    }

    if (origin == "cquest") {
        origin = "https://data.cquest.org/ign/adminexpress"
    }

    to_do = data.frame(
        origin = origin,
        destination = destination,
        short = "download_admin_express",
        stringsAsFactors = FALSE
    )

    download_archive(
        to_do,
        extension = extension,
        name = name,
        date = date,
        version = version,
        verbose = verbose
    )
}