#' @title Analyse dwellings in the BD Topo v3 reporting some elementary results
#'
#' @description
#' The function performs several analysis in order to check the validity and the
#' consistency of the calculations made by functions `extract_building()` and
#' `extract_dwelling()` over the BD Topo (version 3). The function focuses
#' by default on residential constructions. In addition to returning a list, the
#' function creates tables in csv format and png image files in path.
#'
#' @param origin character, path to the directory where archive are stored.
#' @param destination character, path to the directory where output should be
#' saved. The function expects it to be the same provided to function.
#' `extract_dwelling()` if it has been previously invoked in the analysis.
#' Default to same path as origin but substituting automatically keyword
#' 'building' by keyword 'dwelling' when possible
#' @param archive character, vector of filenames to be processed. If missing,
#' analyse_dwelling is recursively called on all admissible archives found in
#' origin.
#' @param category character, focus of the analysis. Default to "Résidentiel".
#' @param dwelling_mean_surf numeric, average surface of a dwelling in France.
#' Default to 90 m^2, which is from adapted to the GT AMC's methodology.
#' @param report logical, should the function create a summary report with the
#' outputs. Default to TRUE.
#' @param retrieve logical, should the output of the function be returned.
#' @param verbose logical, should the function send some messages while running.
#'
#' @return A list of analysis if 'retrieve = TRUE'.
#'
#' @examples
#' \dontrun{
#'
#' # calling function. Long version
#'    tab_building = analyse_dwelling(
#'        archive = "bdtopo-building-2022-03-15-D034.rds",
#'        origin = path_building,
#'        destination = path_dwelling,
#'        category = "Résidentiel",
#'        vintage = "2022-03-15"
#'    )
#'
#' # calling function. short version
#'    tab_building = analyse_dwelling(
#'        archive = "bdtopo-building-2022-03-15-D034.rds",
#'        origin = path_building,
#'        destination = path_dwelling
#'    )
#'
#' # calling function. recursive version
#'    analyse_dwelling(origin = path_building)
#' }
#'
#' @export
#'
#' @encoding UTF-8
#' @author David Nortes Martinez et Frédéric Grelot

analyse_dwelling = function(
    origin,
    destination = gsub("building", "dwelling", origin),
    archive,
    category = "R\u00E9sidentiel",
    dwelling_mean_surf = (90 * 1.2),
    report = TRUE,
    retrieve = TRUE,
    verbose = TRUE

){
    # Recursive call if archive is missing
    if (missing(archive)) {archive = list.files(origin, pattern = ".rds$")}
    if (length(archive) > 1) {
        # Some message TO BE CHANGED WITH LOG!!!
        if (verbose == TRUE) {
            msg = sprintf(
                "analyse_dwelling applied to %s (%s archives in progress)...",
                origin,
                length(archive)
            )
            message(msg)
        }
        return(
            invisible(
                lapply(
                    archive,
                    analyse_dwelling,
                    origin = origin,
                    destination = destination,
                    category = category,
                    dwelling_mean_surf = dwelling_mean_surf,
                    report = report,
                    retrieve = retrieve,
                    verbose = verbose
                )
            )
        )
    }
    if (length(archive) == 0)
        return(NULL)

    archive = file.path(origin, archive)
    dwelling_name = gsub("building", "dwelling", basename(archive))
    if (verbose == TRUE) {
        message(sprintf("\t- %s:", dwelling_name))
    }

    # Checking if parameters are supplied and assigning values if not
    param = strsplit(
        x = basename_core(archive),
        split = "-"
    )[[1]]
    vintage = sprintf("%s-%s-%s", param[3], param[4], param[5])
    department = param[6]
    output_name = sprintf("bdtopo-dwelling-%s-%s", vintage, department)
    dwelling_path = file.path(destination, dwelling_name)

    # Checking if dataset from 'extract_dwelling exists'. Creating it if not
    if (!file.exists(dwelling_path)) {
        extract_dwelling(origin, destination, basename(archive))
    }

    # Loading datasets & adding information to dataset
    building = read_format_rds(archive)
    dwelling = read_format_rds(dwelling_path)
    msg_size = sprintf(
        "%s dwellings (from %s buildings)",
        nrow(dwelling),
        nrow(building)
    )

    # Checking if sufficient data in dwelling
    if (all(is.na(dwelling[["typology"]]))) {
        # Some message TO BE CHANGED WITH LOG!!!
        if (verbose == TRUE) {
            msg = c(
                sprintf("\t\t- None of the %s has a typology set.\n", msg_size),
                "\t\t- Analysis is aborted."
            )
            message(msg)
        } else {
            msg = c(
                "analyse_dwelling:\nAnalysis aborted on ",
                sprintf(
                    "%s. None of the %s as a typology set.",
                    gsub("building", "dwelling", archive),
                    msg_size
                )
            )          
            warning(msg, call. = FALSE)
        }
        return(invisible(NULL))
    }

    # Create folders
    dir.create(file.path(destination, "figure"), FALSE, TRUE)
    dir.create(file.path(destination, "table"), FALSE, TRUE)
    dir.create(file.path(destination, "analysed"), FALSE, TRUE)
    output_rds = file.path(
        destination,
        "analysed",
        sprintf("%s-analysed.rds", output_name)
    )

    # Prepare tables from building and dwelling
    result = c(
        prepare_table_building(
            dataset = building,
            category
        ),
        prepare_table_dwelling(
            dataset = dwelling,
            category
        )
    )

    # exploratory plots
    plot_construction_date(
        dataset = dwelling,
        output_name = output_name,
        destination = destination
    )
    qq_height_level(
        dataset = dwelling,
        output_name = output_name,
        destination = destination
    )
    qq_developed_dwelling(
        dataset = dwelling,
        output_name = output_name,
        destination =  destination,
        dwelling_mean_surf = dwelling_mean_surf
    )

    # saving results
    lapply(
        names(result),
        function(x) {
            utils::write.csv(
                result[[x]],
                file.path(
                    destination,
                    "table",
                    sprintf("%s-%s-%s.csv", output_name, category, x)
                )
            )
        }
    )

    # additional information and formatting when report = TRUE
    if (report) {
        # initialize list
        report = list()

        # storing parameters
        report[["department"]] = department
        report[["v_bdtopo"]] = vintage
        report[["output_name"]] = "dwelling"
        report[["report_name"]] = sprintf("%s-report", output_name)

        # storing file path to plots generated above
        report[["plots"]] = file.path(
            normalizePath(
                list.files(
                    file.path(destination, "figure"),
                    pattern = sprintf("^%s.*.png", output_name),
                    full.names = TRUE,
                    recursive = TRUE
                )
            )
        )

        if (length(report[["plots"]]) == 0) {report[["plots"]] = NULL}

        # storing file path to maps
        report[["map_build"]] = file.path(
            normalizePath(
                list.files(
                    origin,
                    pattern = gsub(".rds", ".png", basename(archive)),
                    full.names = TRUE,
                    recursive = TRUE
                )
            )
        )
        report[["map_dwel"]] = file.path(
            normalizePath(
                list.files(
                    destination,
                    pattern = sprintf("^%s.png", output_name),
                    full.names = TRUE,
                    recursive = TRUE
                )
            )
        )
        report[["map_dwel-nc"]] = file.path(
            normalizePath(
                list.files(
                    destination,
                    pattern = sprintf("^%s-nc.png", output_name),
                    full.names = TRUE,
                    recursive = TRUE
                )
            )
        )

        field = c("nb_dwelling", "dwelling_info_%", "roof", "wall")
        if (all(field %in% names(result))) {
            # pre-format layout of table 2 building
            temp05 = result[["nb_dwelling"]][, 3]> 0 &
                result[["nb_dwelling"]][, 3] < .5
            temp95 = result[["nb_dwelling"]][, 3]> 99.5 &
                result[["nb_dwelling"]][, 3] < 100
            report[["nb_dwelling_doc"]] = format(
                round(result[["nb_dwelling"]], 0),
                scientific = FALSE,
                nsmall = 0,
                big.mark = " ",
                decimal.mark = ","
            )
            report[["nb_dwelling_doc"]][, 3][temp05] = "< 0.5"
            report[["nb_dwelling_doc"]][, 3][temp95] = "> 99.5"

            # pre-format layout of table 3 building
            temp05 = result[["dwelling_info_%"]][ , -1] > 0 &
                result[["dwelling_info_%"]][ , -1]  < .5
            temp95 = result[["dwelling_info_%"]][ , -1] > 99.5 &
                result[["dwelling_info_%"]][ , -1] < 100
            report[["dwelling_info_doc"]] = format(
                round(result[["dwelling_info_%"]], 0),
                scientific = FALSE,
                nsmall = 0,
                big.mark = " ",
                decimal.mark = ","
            )
            report[["dwelling_info_doc"]][ , -1][temp05] = "< 0.5"
            report[["dwelling_info_doc"]][ , -1][temp95] = "> 99.5"

            # pre-format layout of tables for roofs and walls
            for (i in c("roof", "wall")){
                temp05 = result[[i]] > 0 & result[[i]] < .5
                temp95 = result[[i]] > 99.5 & result[[i]] < 100
                report[[sprintf("%s_doc", i)]] = format(
                    round(result[[i]], 0),
                    scientific = FALSE,
                    nsmall = 0,
                    big.mark = " ",
                    decimal.mark = ","
                )
                report[[sprintf("%s_doc", i)]][temp05] = "< 0.5"
                report[[sprintf("%s_doc", i)]][temp95] = "> 99.5"
                report[[sprintf("%s_doc", i)]] = cbind(
                    "mat\u00E9riaux" =
                        dimnames(report[[sprintf("%s_doc", i)]])[[1]],
                    report[[sprintf("%s_doc", i)]]
                )
            }

            # pre-format layout of table 3 building
            temp05 = result[["wallroof_comb"]][ , -1] > 0 &
                result[["wallroof_comb"]][ , -1] < .5
            temp95 = result[["wallroof_comb"]][ , -1] > 99.5 &
                result[["wallroof_comb"]][ , -1] < 100
            report[["wallroof_comb_doc"]] = format(
                round(result[["wallroof_comb"]], 0),
                scientific = FALSE,
                nsmall = 0,
                big.mark = " ",
                decimal.mark = ","
            )
            report[["wallroof_comb_doc"]][ , -1][temp05] = "< 0.5"
            report[["wallroof_comb_doc"]][ , -1][temp95] = "> 99.5"

            # arranging 'wall' and 'roof' tables in one single table taking into
            # account potential length differences between them
            if (nrow(result[["roof"]]) != nrow(result[["wall"]])) {
                aux = report[c("wall_doc", "roof_doc")]
                gap = abs(diff(unlist(lapply(aux, nrow))))
                index = which.min(lapply(aux, nrow))
                aux[[index]] = rbind(
                    aux[[index]][-nrow(aux[[index]]), ],
                    matrix(
                        rep(" ", 2*gap),
                        ncol = 2
                    ),
                    aux[[index]][nrow(aux[[index]]), ]
                )
                report[["wall_roof_doc"]] = Reduce(cbind, aux)
                report[c("wall_doc", "roof_doc")] = NULL
            } else {
                report[["wall_roof_doc"]] = Reduce(
                    cbind,
                    result[c("wall", "roof")]
                )
            }
        }
    }
    result[["report"]] = report

    saveRDS(object = result, file = output_rds)

    # Some message TO BE CHANGED WITH LOG!!!
    if (verbose == TRUE) {
        msg = sprintf(
            "\t\t-%s analyses performed on %s.",
            length(result),
            msg_size
        )
        message(msg)
    }

    if (retrieve) {
        return(invisible(result))
    }
}

#' @title Analyse BD Topo before classification of dwellings
#'
#' @description
#' The function performs several analysis in order to check the validity and the
#' consistency of the calculations made by functions `extract_building()` over
#' the BDTopo v3.
#'
#' @param dataset dataframe.
#' @param category character, focus of the analysis. Default value to
#' "Résidentiel".
#'
#' @return A list.
#'
#' @encoding UTF-8
#' @author David Nortes Martinez

prepare_table_building = function(dataset, category) {

    #initializing lists
    result = list()
    temp = list()

    # calculating total of polygons by category in variable "classification_fr"
    temp[["total"]] = table(dataset[["classification_fr"]], useNA = "no")

    # Selection of dwellings
    selection = (!is.na(dataset[["destination_principal"]]) &
        dataset[["destination_principal"]] == category) |
        (!is.na(dataset[["destination_secondary"]]) &
        dataset[["destination_secondary"]] == category) |
        (!is.na(dataset[["dwelling"]]) &
        dataset[["dwelling"]] > 0)
    
    # calculating total of polygons whose either main or secondary destination
    # is residential by category in variable "classification_fr"
    temp[["logement"]] = table(dataset[selection, "classification_fr"], useNA = "no")

    # calculating disaggregation of residences by destination and dwelling per
    # category of variable "classification_fr"
    field = c("destination_principal", "destination_secondary", "dwelling")
    temp = c(
        temp,
        sapply(
            field,
            function(x) {
                if (x == "dwelling"){
                    table(
                        dataset[
                            selection & dataset[[x]] > 0,
                            "classification_fr"
                        ],
                        useNA = "no"
                    )
                    } else {
                    table(
                        dataset[
                            selection & dataset[[x]] == category,
                            "classification_fr"
                        ],
                        useNA = "no"
                    )
                    }
            },
            USE.NAMES = TRUE,
            simplify = FALSE
        )
    )
    names(temp)[-c(1:2)] = c("principal", "secondaire", "dwelling")

    # Number of logements by aggregation of principal and secondary
    temp[["destination"]] = temp[["principal"]] + temp[["secondaire"]]

    # Number of "dwellings" by category in variable "classification_fr"
    aux = stats::aggregate(
        dwelling ~ classification_fr,
        dataset[selection, ],
        sum,
        na.rm = TRUE,
        drop = FALSE
    )
    aux[is.na(aux)] = 0
    temp[["nb_dwellings"]] = aux[ , 2]
    names(temp[["nb_dwellings"]]) = aux[ , 1]

    # coercing to matrix
    aux = Reduce(cbind, temp)
    dimnames(aux)[[2]] = names(temp)
    temp = list(
        "general" = aux[
            ,
            c(
                "total",
                "logement",
                "destination",
                "principal",
                "secondaire",
                "dwelling",
                "nb_dwellings"
            )
        ]
    )

    # calculating totals by column in temp
    temp[["general"]] = rbind(
        temp[["general"]],
        total = colSums(temp[["general"]])
    )

    # grouping columns for outputs
    aux = temp[["general"]][,"logement"] * 100 / temp[["general"]][,"total"]
    nb_dwelling = cbind(temp[["general"]], "%" = aux)
    nb_dwelling = nb_dwelling[ , c(1:2, 8, 3:7)]
    dimnames(nb_dwelling)[[2]][c(2, 4, 7:8)] = 
        c("polygon", "ensemble", "polygon", "nombre")
    result[["nb_dwelling"]] = nb_dwelling

    # calculating number of dwellings by whether destination is filled in
    destination_residentiel = 
        (!is.na(dataset[["destination_principal"]]) &
            dataset[["destination_principal"]] == category) |
        (!is.na(dataset[["destination_secondary"]]) &
            dataset[["destination_secondary"]] == category)
    dwellings = (!is.na(dataset[["dwelling"]]) & dataset[["dwelling"]] > 0)
    result[["dest_residentiel-dwelling"]] = table(
        destination_residentiel,
        dwellings,
        useNA = 'always'
    )

    ## disaggregating by category
    aux = sapply(
        levels(dataset[["classification_fr"]]),
        function(x){
            dataset = dataset[dataset[["classification_fr"]] == x, ]
            destination_residentiel = factor(
                (!is.na(dataset[["destination_principal"]]) &
                dataset[["destination_principal"]] == category) |
                (!is.na(dataset[["destination_secondary"]]) &
                dataset[["destination_secondary"]] == category),
                levels = c(TRUE, FALSE)
            )
            dwellings = factor(
                (!is.na(dataset[["dwelling"]]) & dataset[["dwelling"]] > 0),
                levels = c(TRUE, FALSE)
            )
            temp = table(
                destination_residentiel,
                dwellings,
                useNA = 'always'
            )
            return(c(temp[2, 1], temp[1, 2]))
        },
        simplify = TRUE
    )
    dimnames(aux)[[1]] = c("T-F","F-T")
    aux = cbind(aux, total = rowSums(aux))
    result[["nb_dwelling"]] = cbind(result[["nb_dwelling"]], t(aux))

    # checkups table
    result[["checkups_tab1"]] = matrix(
        c(
            sum(is.na(dataset[["nature"]])),
            sum(is.na(dataset[["destination_principal"]])),
            sum(
                (!is.na(dataset[["destination_principal"]]) &
                dataset[["destination_principal"]] == category) &
                (!is.na(dataset[["destination_secondary"]]) &
                dataset[["destination_secondary"]] == category)
            )
        ),
        nrow = 1,
        byrow = TRUE,
        dimnames = list(
            c(""),
            c(rep("NA", 2),
            "doublons")
        )
    )
    result[["checkups_tab2"]] = matrix(
        c(
            result[["dest_residentiel-dwelling"]][2, 1],
            result[["dest_residentiel-dwelling"]][1, 2]
        ),
        nrow = 1,
        byrow = TRUE,
        dimnames = list(
            c(""),
            c("TRUE - FALSE",
            "FALSE - TRUE")
        )
    )

    return(result)
}

#' @title Analyse BD Topo after classification of dwellings
#'
#' @description
#' The function performs several analysis in order to check the validity and the
#' consistency of the calculations made by function `extract_dwelling` over the
#' BDTopo v3.
#'
#' @param dataset dataframe.
#' @param category character, focus of the analysis. Default value to
#' "Résidentiel".
#'
#' @return A list.
#'
#' @encoding UTF-8
#' @author David Nortes Martinez

prepare_table_dwelling = function(dataset, category) {

    # checking if the "typology" variable is a factor. Coercing to factor if not
    if (is.character(dataset[["typology"]])) {
        dataset[["typology"]] = as.factor(dataset[["typology"]])
    }

    # initializing lists
    result = list()
    temp = list()

    # Selection
    selection = (!is.na(dataset[["destination_principal"]]) &
        dataset[["destination_principal"]] == category) |
        (!is.na(dataset[["destination_secondary"]]) &
        dataset[["destination_secondary"]] == category) |
        (!is.na(dataset[["dwelling"]]) &
        dataset[["dwelling"]] > 0)

    # calculating number of polygons included in each category of variable "typology"
    #aux = list("dwelling" = result[["general"]][,"dwelling"])
    aux = list("polygon" = as.matrix(
        table(
            dataset[selection, "classification_fr"],
            useNA = "no"
        )
    ))
    aux[["typology"]] = table(
        dataset[selection, c("classification_fr", "typology")],
        useNA = "always"
    )
    aux[["typology"]] = aux[["typology"]][-nrow(aux[["typology"]]), ]
    aux = Reduce(cbind, aux)
    aux = rbind(
        aux,
        total = colSums(aux, na.rm = TRUE)
    )
    dimnames(aux)[[2]][c(1, ncol(aux))] = c("polygon", "NC")
    temp[["dwelling_type"]] = aux

    # calculating number of "dwellings" by category in variable "classification_fr" and dwelling type
    aux = stats::aggregate(
        dwelling ~ classification_fr + typology,
        dataset[selection, ],
        sum,
        na.rm = TRUE,
        drop = FALSE
    )
    aux[is.na(aux)] = 0
    aux = stats::reshape(
        aux,
        direction = "wide",
        idvar = "classification_fr",
        timevar = "typology"
    )
    row.names(aux) = aux[ , 1]
    aux = rbind(
        as.matrix(aux[ , -1]),
        total = colSums(aux[ , -1], na.rm = TRUE)
    )
    dimnames(aux)[[2]] = gsub("dwelling.","nb_dwel.", dimnames(aux)[[2]])
    aux = cbind(
        aux,
        total = rowSums(aux, na.rm = TRUE)
    )
    result[["dwelling_type"]] = cbind(temp[["dwelling_type"]], aux)

    # calculating total of polygons whose either main or secondary destination
    # is residential by category in variable "classification_fr"
    temp[["polygon"]] = table(dataset[selection, "typology"], useNA = "always")

    # Calculating total of polygons whose destination is residential according
    # to categories of "state" variable
    temp[["inactif"]] = table(
        dataset[selection & (!is.na(dataset[["state"]]) & 
        dataset[["state"]] != "En service"), "typology"],
        useNA = "always"
    )

    # Polygons considered  as lightweight constructions
    temp[["l\u00E9g\u00E8re"]] = table(
        dataset[selection & (!is.na(dataset[["building_light"]]) & 
        dataset[["building_light"]] == TRUE), "typology"],
        useNA = "always"
    )

    # Polygons located within eaip
    temp[["eaip"]] = table(
        dataset[selection & (!is.na(dataset[["eaip"]]) & 
        dataset[["eaip"]] == TRUE), "typology"],
        useNA = "always")

    # Polygons withconstruction date filled in
    dataset[["date_apparition_logical"]] = !is.na(dataset[["date_apparition"]])
    temp[["date"]] = table(
        dataset[selection & (!is.na(dataset[["date_apparition_logical"]]) & 
        dataset[["date_apparition_logical"]] == TRUE), "typology"],
        useNA = "always"
    )

    # Polygons with construction materials for walls filled in
    dataset[["wall_logical"]] =
        dataset[["material_wall"]] %in%
        levels(dataset[["material_wall"]])[-c(1:2)]

    temp[["mur"]] = table(
        dataset[selection & (!is.na(dataset[["wall_logical"]]) & 
        dataset[["wall_logical"]] == TRUE), "typology"],
        useNA = "always"
    )

    # Polygons with construction materials for roof filled in
    dataset[["roof_logical"]] =
        dataset[["material_roof"]] %in%
        levels(dataset[["material_roof"]])[-c(1:2)]

    temp[["toiture"]] = table(
        dataset[selection & (!is.na(dataset[["roof_logical"]]) & 
        dataset[["roof_logical"]] == TRUE), "typology"],
        useNA = "always"
    )

    # Polygons with with good (majic+) and not so good (majic-)
    dataset[["majic_bd_correspondence"]] = gsub(
        "[0-9 .]",
        "",
        dataset[["majic_quality"]]
    )
    dataset[["majic_bd_correspondence"]][
        dataset[["majic_bd_correspondence"]] %in% c("A", "B")
    ] = "majic+"
    dataset[["majic_bd_correspondence"]][
        dataset[["majic_bd_correspondence"]] == "C"
    ] = "majic-"


    temp[["majic+"]] = table(
        dataset[
            selection & (!is.na(dataset[["majic_bd_correspondence"]]) & 
            dataset[["majic_bd_correspondence"]] == "majic+"), "typology"
        ],
        useNA = "always"
    )

    temp[["majic-"]] = table(
        dataset[
            selection & (!is.na(dataset[["majic_bd_correspondence"]]) & 
            dataset[["majic_bd_correspondence"]] == "majic-"), "typology"
        ],
        useNA = "always"
    )

    # Polygons whose number of floors is filled in
    dataset[["level_logical"]] = !is.na(dataset[["level"]])

    temp[["level"]] = table(
        dataset[selection & (!is.na(dataset[["level_logical"]]) & 
        dataset[["level_logical"]] == TRUE), "typology"],
        useNA = "always"
    )

    # Polygons whose height is filled in
    dataset[["height_logical"]] = !is.na(dataset[["height"]])

    temp[["height"]] = table(
        dataset[selection & (!is.na(dataset[["height_logical"]]) & 
        dataset[["height_logical"]] == TRUE), "typology"],
        useNA = "always"
    )

    # Coercing to matrix
    field = c(
        "polygon",
        "inactif",
        "l\u00E9g\u00E8re",
        "eaip",
        "date",
        "mur",
        "toiture",
        "majic+",
        "majic-",
        "level",
        "height"
    )
    aux = Reduce(cbind, temp[field])
    aux = rbind(aux, total = colSums(aux))
    dimnames(aux) = list(
        c(levels(dataset[["typology"]]), "NC", "total"),
        field
    )

    # calculating totals by column in result
    result[["dwelling_info"]] = aux

    # calculating percentages
    aux[aux[ , 1] > 0, -1] = (aux[aux[ , 1] > 0, -1]*100)/aux[aux[ , 1] > 0, 1]
    result[["dwelling_info_%"]] = aux

    # calculating totals by material on walls and roofs
    for (i in c("wall", "roof")) {
        aux = table(
            dataset[selection, sprintf("label_%s_fr", i)],
            useNA = "always"
        )
        names(aux)[length(aux)]="NA"
        aux["indetermine+NA"] = aux["indetermine"] + aux["NA"]
        aux = c(
            aux[names(aux)[
                !names(aux) %in% c(
                    "indetermine",
                    "autres",
                    "indetermine+NA",
                    "NA"
                )
            ]],
            aux["autres"],
            aux["indetermine+NA"]
        )
        aux = aux * 100 / result[["dwelling_type"]]["total","polygon"]
        result[[i]] = as.matrix(aux[names(aux)!="NA"])
        dimnames(result[[i]])[[2]] = "%"
    }

    # calculating totals by combined material on walls and roofs
    for (i in c("wall", "roof")) {
        aux = table(
            dataset[selection, sprintf("combined_%s", i)],
            useNA = "always"
        )
        names(aux) = c("F","T","indetermine+NA")
        aux = c(
            aux["T"],
            aux["F"],
            aux["indetermine+NA"]
        )
        aux = aux * 100 / result[["dwelling_type"]]["total","polygon"]
        result[[sprintf("%s_comb", i)]] = t(as.matrix(aux))
        dimnames(result[[sprintf("%s_comb", i)]])[[1]] = i
    }
    result[["wallroof_comb"]] = Reduce(
        rbind,
        result[c("wall_comb","roof_comb")]
    )
    result[c("wall_comb","roof_comb")] = NULL

    return(result)
}

#' @title Read archive (building and dwelling) and format in a convenient way
#'
#' @description
#' Read and format archive (building and dwelling) with global variables
#' floodam.data::bd_topo_nature_reclass, floodam.data::bd_topo_wall, and
#' floodam.data::bd_topo_roof.
#'
#' @param x character, path of th archive.
#'
#' @return data.frame
#'
#' @encoding UTF-8
#' @author David Nortes Martinez

read_format_rds = function(x) {
    result = readRDS(x)
    
    # Drop geometry
    result = sf::st_drop_geometry(result)

    # COMMENT HERE
    result = Reduce(
        function(u, v) {merge(u, v, all.x = TRUE)},
        list(
            result,
            floodam.data::bd_topo_nature_reclass,
            floodam.data::bd_topo_wall,
            floodam.data::bd_topo_roof
        )
    )

    # Ordering  classification_fr
    result[["classification_fr"]] = factor(
        result[["classification_fr"]],
        levels = unique(
            as.character(
                floodam.data::bd_topo_nature_reclass[["classification_fr"]][
                    order(floodam.data::bd_topo_nature_reclass[["order"]])
                ]
            )
        )
    )
    return(result)
}