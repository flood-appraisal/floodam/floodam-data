#' @title Update state from observation and BAN 
#' 
#' @description
#' `update_state()` creates a new state of observations in an area. All rows
#' (address) of the BAN are kept. For each address that has also observations
#' both valid and consistent with BAN's version, informations from the most
#' recent observation is added. A summary is added in the column `state` is
#' also given to easily find if an address has already been observed or not, and
#' if observed if it has protection or not.
#'
#' @param observation data.frame of observations.
#' @param ban data.frame of address from BAN.
#' @param observation_field character, columns to be kept in observation.
#' @param ban_field character, columns to be kept in ban.
#'
#' @return data.frame consistent with ban data.frame (same rows) and with
#' informations from most recent observations.
#'
#' @export
#'
#' @encoding UTF-8

update_state = function(
    observation,
    ban,
    observation_field = c("ban_id", "date", "visibility", "n_protection",
    "n_building", "n_fence", "coord_sup", "survey_id",
    "campaign", "survey_state"),
    ban_field = c("id", "address_number", "address_rep", "address_street",
    "commune", "position_type", "locality_name")
) {
    col_obs = intersect(observation_field, names(observation))
    if (length(col_obs) != length(observation_field)) {
        warning("Missing column in observations.")
    }
    col_ban = intersect(ban_field, names(ban))
    if (length(col_ban) != length(ban_field)) {
        warning("Missing column in ban.")
    }

    # Correction of data (should not be necessary!)
    observation[is.na(observation[["n_protection"]]), "n_protection"] = 0

    # Select only valid surveys of latest ban_version
    n_obs = nrow(observation)
    selection =
        observation[["ban_version"]] == max(observation[["ban_version"]]) &
        observation[["survey_state"]] == "valid"
    observation = observation[selection, ]
    n_obs_valid = nrow(observation)

    # Select most recent survey for each address
    o = order(observation[["date"]], observation[["ban_id"]])
    observation = observation[o, ]
    selection = ! duplicated(observation[["ban_id"]], fromLast = TRUE)
    observation = observation[selection, ]
    n_obs_final = nrow(observation)
    
    observation = observation[col_obs]
    ban = ban[col_ban]
    names(ban)[names(ban) == "id"] = "ban_id"
    result = merge(ban, observation, by = "ban_id", all.x = TRUE)

    result["state"] = ifelse(result[["n_protection"]] > 0, "with", "none")
    result[["state"]][is.na(result[["state"]])] = "unobserved"

    return(result)
}