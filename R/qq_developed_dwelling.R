#' @title qq plot of the building's developed surface versus the building's 
#' dwellings in the context of the BD-Topo v3
#' 
#' @description
#' The function produces a QQ-plot that confronts the following series of the 
#' building/dwelling database: surface developed and number of dwellings. The 
#' plot is stored as png image in destination 
#' 
#' @param output_name character. 
#' @param dataset data.frame
#' @param destination character. Path to folder where tables and figures must be stocked.
#' @param dwelling_mean_surf numeric. Average surface of a dwelling
#' 
#'
#' @export
#'
#' @encoding UTF-8
#' @author David Nortes Martinez

qq_developed_dwelling = function(
    output_name, 
    dataset, 
    destination, 
    dwelling_mean_surf
){
    
    # removing units
    dataset[["developed"]] = units::set_units(dataset[["developed"]], NULL)
    
    # generating average series
    mean_series = dwelling_mean_surf * seq(
        0:max(dataset[["dwelling"]], 
        na.rm = TRUE)
        )
    
    # color vector
    color = rep(
        scales::alpha("black", 0.35), 
        nrow(dataset)
    )
    color[dataset[["developed"]] == 0] = scales::alpha("red", 0.4)
    color[dataset[["dwelling"]] == 0] = scales::alpha("dodgerblue", 0.4)

    # regression model
    model = stats::lm(dwelling ~ developed, data = dataset)
    # generating and saving plot in destination
    grDevices::png(
        file.path(
            destination, 
            "figure", 
            paste0(output_name, "-", "developed-vs-dwelling.png")
        ),
        height = 360, 
        width = 720
    )

        graphics::par(mar = c(5,5,4,2))
        plot(
            x = dataset[["developed"]],
            y = dataset[["dwelling"]],
            pch = 20,
            cex = 1,
            xlab = "Surface d\u00E9velopp\u00E9e (m\u00B2)",
            ylab = "Nombre de logements",
            col = color,
            cex.lab = 1.8,
            cex.main = 1.8,
            cex.axis = 1.8,
            xaxt = "n"
            )
        graphics::axis(
            side=1, 
            at=seq(0, max(dataset[["developed"]], na.rm = TRUE), by = 20000), 
            labels=format(
                seq(0, max(dataset[["developed"]], na.rm = TRUE), by = 20000), 
                big.mark = " "
                ),
            cex.axis = 1.8
        )
        graphics::lines(
            x = mean_series,
            y = seq(0:max(dataset[["dwelling"]], na.rm = TRUE)),
            cex = .2,
            col = "limegreen"
        )
        graphics::lines(
            x = mean_series/2,
            y = seq(0:max(dataset[["dwelling"]], na.rm = TRUE)),
            lty = "dashed",
            cex = .3,
            col = scales::alpha("limegreen", 1)
        )
        graphics::lines(
            x = mean_series*2,
            y = seq(0:max(dataset[["dwelling"]], na.rm = TRUE)),
            lty = "dashed",
            cex = .3,
            col = scales::alpha("limegreen", 1)
        )
        graphics::points(
            x = dataset[dataset[["dwelling"]] == 0, "developed"],
            y = dataset[dataset[["dwelling"]] == 0, "dwelling"],
            pch = 20,
            cex = 1,
            col = color[dataset[["dwelling"]] == 0]
        )
        graphics::points(
            x = dataset[dataset[["developed"]] == 0, "developed"],
            y = dataset[dataset[["developed"]] == 0, "dwelling"],
            pch = 20,
            cex = 1,
            col = color[dataset[["developed"]] == 0]
        )
        graphics::abline(model, col = "darkorange")
        graphics::legend(
            "bottomright",
            bty = "n",
            lty=c(rep(1,2),2,rep(NA,3)),
            pch = c(rep(NA,3), rep(20,3)),
            col = c(
                "darkorange",
                rep("limegreen",2),
                scales::alpha("black", 0.35), 
                scales::alpha("dodgerblue", 0.4), 
                scales::alpha("red",0.4)
                ),
            legend = format(
                c(
                    "mod\u00E8le lin\u00E9aire",
                    "moyenne (90m\u00B2)",
                    "0,5x|2x moyenne",
                    sum(

                        (!is.na(dataset[["developed"]] & dataset[["developed"]]>0)) & 
                        (!is.na(dataset[["dwelling"]]) & dataset[["dwelling"]]>0), 
                        na.rm = TRUE
                    ),
                    sprintf(
                        "nb. dwel. = 0: %i", 
                        sum(
                        (!is.na(dataset[["dwelling"]]) & dataset[["dwelling"]]==0) & 
                        !is.na(dataset[["developed"]]), 
                        na.rm = TRUE
                        )
                    ),
                    sprintf(
                        "s. dev. = 0: %i",
                        sum(
                        (!is.na(dataset[["developed"]]) & dataset[["developed"]]==0) & 
                        !is.na(dataset[["dwelling"]]), 
                        na.rm = TRUE
                        )
                    )
                ),
                big.mark = " "
            ),
            cex = 1.8
        )
    grDevices::dev.off()
}