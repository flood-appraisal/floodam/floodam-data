#' Prepare a pattern for grep
#'
#' Prepare a pattern from a vector of characters. It particularly useful to
#' find files of given extension within a vector of names.
#'
#' @param x character, vector of patterns to be completed
#' @param begin logical, should the pattern be at the beginning of strings
#' @param ending logical, should the pattern be at the end of strings
#' @param extension logical, should the pattern considered as an extension with a starting dot
#'
#' @return character of length one. To each element of x, it is firts applied transformation given by
#' begin, ending and extention. Then, it is collapsed with "|". It can be used in a grep call.
#'
#' @export
#'
#' @examples
#' extension = c("zip", "7z")
#' complete_pattern(extension)
#' complete_pattern(extension, extension = TRUE)
#' complete_pattern(extension, ending = TRUE)

complete_pattern = function(x, begin = FALSE, ending = FALSE, extension = FALSE) {
    if(extension) x = paste("[.]", x, sep = "")
    if(begin)     x = paste("^", x, sep = "")
    if(ending)    x = paste(x, "$", sep = "")
    return(paste(x, collapse = "|"))
}