#' @title Catch and interpret arguments for a R script
#' 
#' @description
#' `interpret_arg()` catch arguments through `commandArgs()` and interpret them
#' to return a list of arguments for those that are of a "good shape".
#' 
#' @param x character of parameters to be treated. Default to
#' `commandArgs(trailingOnly = TRUE)`.
#' @return A list of arguments
#'
#' @export
#' 
#' @examples
#' 
#' x = c(1, "x=2", "y='a=1'")
#' interpret_args(x)

interpret_args = function(x = commandArgs(trailingOnly = TRUE)) {
    # result = strsplit(commandArgs(trailingOnly = TRUE), "=")
    result = strsplit(x, "=")

    selection = lengths(result) > 2
    result[selection] = lapply(
        result[selection],
        function(x) {c(x[1], paste(x[-1], collapse = "="))}
    )

    selection = lengths(result) == 2
    names(result)[selection] = lapply(result[selection], `[[`, 1)
    result[selection] = lapply(result[selection], `[[`, 2)

    names(result)[!selection] = ""
    result
}
