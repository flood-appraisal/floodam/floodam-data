#' @title Summarise dwelling information from BD Topo
#' 
#' @description
#' `summarise_dwelling()` creates a summary of information that should
#' be for dwellings. It performs elementary statistics on how many buildings and
#' dwellings are included for a structured typlogy used inside the function.
#'
#' @details
#' If the option flood is given, `summarise_dwelling()` first makes a
#' selection on this column before doing the analysis.
#' 
#' If the option graph is set to TRUE, `summarise_dwelling()` traces graphs
#' that represent how "wall", "roof", and "level" evolves in yerly based for
#' each type in "AB", "MFH", "TH", and "SH". Those graphs are saved with a
#' reference to "flood" if given, to be able to find for what selection those
#' graphs have been plotted. If "flood" is not given, the reference is "all".
#'
#' @param origin character, path to the directory where archive are stored.
#' @param destination character, path to the directory where results should be
#' saved.
#' @param archive character, vector of archive to be summarised.
#' @param flood character, name of column that give it dwellings are in a flood
#' area. This column should be of type logical.
#' @param graph logical, should graphs be plotted.
#' @param retrieve logical, should the result be returned.
#' @param verbose logical, should the function give some sumup informations.
#'
#' @return if retrieve = TRUE, data.frame of summarised data.
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot

summarise_dwelling = function(
    origin,
    destination = file.path(origin, "synthesis"),
    archive,
    flood = NULL,
    graph = TRUE,
    retrieve = FALSE,
    verbose = TRUE
) {
    ###  Recursive call if archive is missing
    if (missing(archive)) {archive = list.files(origin, pattern = ".rds$")}
    if (length(archive) > 1) {
        # Some message TO BE CHANGED WITH LOG!!!
        if (verbose == TRUE) {
            msg = sprintf(
                "summarise_dwelling applied to %s (%s archives in progress)...",
                origin,
                length(archive)
            )
            message(msg)
        }
        return(
            invisible(
                lapply(
                    archive,
                    summarise_dwelling,
                    origin = origin,
                    destination = destination,
                    flood = flood,
                    graph = graph,
                    retrieve = retrieve,
                    verbose = verbose
                )
            )
        )
    }
    if (length(archive) == 0)
        return(NULL)

    archive = file.path(origin, archive)
    if (verbose == TRUE) {
        message(sprintf("\t- %s:", basename(archive)))
    }

    x = readRDS(archive)
    if (methods::is(x, "sf")) {x = sf::st_drop_geometry(x)}
    if (!is.null(flood)) {
        if (!flood %in% names(x)) {
            # Some message TO BE CHANGED WITH LOG!!!
            if (verbose == TRUE) {
                msg = sprintf(
                    "\t\t- Aborted ('%s' not in names)",
                    flood
                )
                message(msg)
                return(invisible(NULL))
            }
        }
        x = x[x[[flood]], ]
    } else {
        flood = "all"
    }

    # Classification type
    x[["type"]] = addNA(
        factor(x[["typology"]] , levels = c("AB", "MFH", "TH", "SFH"))
    )

    # Classification wall
    x =  merge(
        x,
        floodam.data::bd_topo_wall[c("material_wall", "label_wall_fr")],
        all.x = TRUE
    )
    x[["wall"]] = addNA(
        factor(
            x[["label_wall_fr"]],
            levels = levels(floodam.data::bd_topo_wall[["label_wall_fr"]])
        )
    )

    # Classification roof
    x =  merge(
        x,
        floodam.data::bd_topo_roof[c("material_roof", "label_roof_fr")],
        all.x = TRUE
    )
    x[["roof"]] = addNA(
        factor(
            x[["label_roof_fr"]],
            levels = levels(floodam.data::bd_topo_roof[["label_roof_fr"]])
        )
    )

    # Classification period
    x[["year"]] = as.numeric(format(x[["date_apparition"]], "%Y"))
    # Original limit from Paul Garcias
    # period_limit = c(0, 1947, 1967, 1974, 1981, 1989, 2000, 2023)
    # INSEE compatible
    period_limit = c(
        0, 1918, 1945, 1970, 1990, 2005, as.integer(format(Sys.Date(), "%Y"))
    )
    level = paste(
        formatC(width = 4, flag = 0, period_limit[-length(period_limit)]),
        period_limit[-1],
        sep = "-"
    )
    x[["period"]] = cut(
        x[["year"]],
        breaks = period_limit,
        labels = paste(
            formatC(width = 4, flag = 0, period_limit[-length(period_limit)]),
            period_limit[-1],
            sep = "-"
        )
    )
    x[["period"]] = addNA(x[["period"]])

    # Classification level
    level_limit = c(0, 1, 3, 100)
    x[["level"]] = cut(
        x[["level"]],
        breaks = level_limit,
        labels = c("1", "2-3", "4+")
    )
    x[["level"]] = addNA(x[["level"]])

    variable = c("department", "type", "level", "period", "wall", "roof")
    x = x[c(variable, "dwelling", "year")]

    result = merge(
        as.data.frame(table(x[variable]), responseName = "building"),
        stats::aggregate(x["dwelling"], x[variable], sum, drop = FALSE, na.rm = TRUE),
        sort = FALSE
    )
    result[["dwelling"]][is.na(result[["dwelling"]])] = 0
    result = result[
        order(
            result[["type"]],
            result[["level"]],
            result[["period"]],
            result[["wall"]],
            result[["roof"]]
        ),
    ]

    # Save result
    dir.create(destination, showWarnings = FALSE, recursive = TRUE)
    name = basename_core(archive)
    data.table::fwrite(
        result,
        file = file.path(destination, sprintf("%s_%s.csv.gz", name, flood)),
        row.names = FALSE
    )

    # Plot graphs
    if (graph == TRUE) {
        path_graph = file.path(destination, "graph")
        dir.create(path_graph, showWarnings = FALSE)
                
        for (what in c("wall", "level", "roof")) {
            for (type in c("AB", "MFH", "SFH", "TH")) {
                temp = x[x[["type"]] %in% type, ]
                plot_prop(
                    temp[c("year", what)],
                    path_output = file.path(
                        path_graph,
                        sprintf("%s_%s_%s_%s.png", name, flood, what, type))
                )
            }
        }
    }

    if (verbose == TRUE) {
        message(
            sprintf(
                "\t\t- Done%s%s.",
                if (graph == TRUE) " with graph" else "",
                if (flood == "all") {
                    " on all data"
                } else {
                    sprintf(" on selected data by %s", flood)
                }
            )
        )
    }
    if (retrieve == TRUE) return(invisible(result))
}

calc_prop = function(x, year = 1900:2020, extent = 5) {
    if (length(year) > 1) {
        return(do.call(rbind, lapply(year, calc_prop, x = x, extent = extent)))
    }

    selection = x[["year"]] %in% (seq(-extent, extent) + year)
    result = as.matrix(table(x[selection, 2], useNA = "always"))
    colnames(result) = year
    t(result)
}

plot_prop = function(x, year = 1900:2020, path_output = NULL) {
    col = scales::alpha(
            c(
            "agglomere" = "#37659a",
            "autres" = "grey90",
            "beton" = "grey30",
            "bois" = "burlywood2",
            "briques" = "firebrick1",
            "indetermine" = "orange",
            "meuliere" = "ivory3",
            "pierre" = "#61523a",
            "ardoises" = "black",
            "tuiles" = "firebrick2",
            "zinc aluminium" = "#34808f",
            "1" = "grey20",
            "2-3" = "grey50",
            "4+" = "grey80"
        ),
        1
    )

    x = calc_prop(x, year = year)
    x = x[, -ncol(x)]
    col = col[colnames(x)]

    if (!is.null(path_output)) {
        grDevices::png(path_output, width = 960, height = 320)
        on.exit(grDevices::dev.off())
    }
    graphics::par(mar = c(5, 4, 4, 2) + 0.1, xpd = TRUE)
    graphics::barplot(t(x/rowSums(x)), col = col, border = NA, space = 0)
    graphics::legend(
        x = "top",
        fill = col,
        legend = colnames(x),
        horiz = TRUE,
        inset = c(0, -0.25)
    )
}
