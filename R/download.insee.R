#' @title Download data from INSEE 
#'
#' @description
#' Function used to download data from INSEE
#'
#' @param destination character, the address where dara are stocked
#' @param origin character, either a keyword or the address from where data are downloaded. Best to keep it
#' at default value. See details.
#' @param name character, vector of acceptable names fo archive to be downloaded
#' @param extension character, vector of acceptable types of archive to be downloaded
#' @param version character, version of archive to be downloaded
#' @param date string, version of archive to be downloaded
#' @param year integer or character, vector of year to be downloaded.
#' @param verbose logical, should the function send somme messages while running.
#'
#' @return nothing
#'
#' @export
#'
#' @examples
#' 
#' \dontrun{download.insee()}

download.insee = function(
    destination,
    origin = "insee",
    name = NULL,
    extension = NULL,
    version = NULL,
    date = NULL,
    year = NULL,
    verbose = TRUE) {
    
    to_do = data.frame(
        origin = origin,
        destination = destination,
        short = "download.insee",
        stringsAsFactors = FALSE
    )

    download_archive(to_do, extension = extension, name = name, date = date, version = version, verbose = verbose)
}