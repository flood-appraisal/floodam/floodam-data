#' @title Download RPG 
#'
#' @description
#' Function used to download RPG
#'
#' @param destination character, the address where dara are stocked.
#' @param origin character, either a keyword or the address from where data are downloaded. See details.
#' @param name character, vector of acceptable names fo archive to be downloaded.
#' @param extension character, vector of acceptable types of archive to be downloaded.
#' @param version string, version of archive to be downloaded.
#' @param date character, date of the archive to be downloaded.
#' @param year integer or character, vector of year to be downloaded.
#' @param verbose logical, should the function send some messages while running.
#'
#' @return nothing
#' 
#' @section Details:
#' 
#' If \code{origin == "cquest"}, then all necessary variables are filled with those values:
#' \itemize{
#'      \item \bold{origin} is changed to \url{https://data.cquest.org/registre_parcellaire_graphique}. 
#'          This url is adapted depending on year.
#'      \item \bold{extension} default value is changed to \code{c("zip", "7z")}.
#'      \item \bold{year} is used to set something equivalent to date, but directly in origin.
#'      \item \bold{date} is set to null, because conflict may occur with year.
#' }
#' 
#' If \code{origin == "IGN"}, then all necessary variables are filled with those values:
#' \itemize{
#'      \item \bold{origin} is changed to \url{ftp://RPG_ext:quoojaicaiqu6ahD@ftp3.ign.fr}.
#'          This url is adapted depending on year.
#'      \item \bold{name} default value is set to "RPG".
#'      \item \bold{extension} default value is changed to \code{c("7z", "7z.001")}.
#'      \item \bold{year} may be used to set something equivalent to date, but directly in origin.
#'          This should be only used for year 2010 and 2011. It is not used if year is something else.
#'      \item \bold{date} default value is set to "last". It is set to NULL if year is successfully used.
#' }
#' 
#' If not, everything shall be filled so that `download_archive()` can make a successful download.
#' 
#'
#' @export
#'
#' @examples
#' 
#' \dontrun{download.rpg(tempfile(), year = "2010")}

download.rpg = function(
    destination,
    origin = "cquest",
    name = NULL,
    extension = NULL,
    version = NULL,
    date = NULL,
    year = NULL,
    verbose = TRUE) {
    if (origin == "cquest") {
        origin = "https://data.cquest.org/registre_parcellaire_graphique"
        if (is.null(year)) {
            x = read_url(origin)
            year = max(unlist(regmatches(x, gregexpr("(?<=href=([\\\"']))\\d{4}(?=\\/\\1)", x, perl = TRUE))))
        }
        origin = if(!is.null(year)) file.path(origin, year) else file.path(origin)
        if (is.null(extension)) extension = c("zip", "7z")
        destination = file.path(destination, year)
        date = NULL
    }
    if (origin == "IGN") {
        origin = "ftp://RPG_ext:quoojaicaiqu6ahD@ftp3.ign.fr"
        if (!is.null(year)) {
            x = read_url(origin)
            year_dir = grep(
                year,
                unlist(regmatches(x, gregexpr("[^ ]*\\d{4}$", x, perl = TRUE))),
                value = TRUE
            )
            if (length(year_dir)) {
                origin = file.path(origin, year_dir)
                destination = file.path(destination, year_dir)
                date = NULL
            } else {
                date = year
            }
        }
        if (is.null(name)) name = "RPG"
        if (is.null(extension)) extension = c("7z", "7z.001")
        if (is.null(date)) date = "last"
    }

    to_do = data.frame(
        origin = origin,
        destination = destination,
        short = "download.admin_express",
        stringsAsFactors = FALSE
    )

    download_archive(to_do, extension = extension, name = name, date = date, version = version, verbose = verbose)
}