#' @title Format scope
#' 
#' @param x vector of scopes.
#' @param type character, either file or something else.
#' @param scope character, either department or region.
#' @param origin character, either department or commune.
#' @param check logical, should check occurs.
#'
#' @return character of formatted scopes
#' 
#' @export
#' 
#' @encoding UTF-8
#' @author Frédéric Grelot
#' 
#' @examples
#' format_scope(1:10)
#' format_scope(1:10, "insee")
#' format_scope(1:10, scope = "region")
#' format_scope(1:10, scope = "region", check = FALSE)
#' format_scope(1:10, "insee", scope = "region")
#' format_scope(c("FR", 1:10), "insee", scope = "region")
#' format_scope(c(1:10, "2A", "002B"))
#' format_scope(c(1:10, "2A", "002B"), "insee")
#' format_scope("R94", "insee", "region")
#' commune = c("01125", "2A123", "97223", "97723")
#' format_scope(commune, "insee", "department", "commune")

format_scope = function(
    x,
    type = c("file", "insee"),
    scope = c("department", "region"),
    origin = c("department", "commune"),
    check = TRUE
) {
    type = match.arg(type)
    scope = match.arg(scope)
    origin = match.arg(origin)
    x = as.character(x)

    if (scope == "department") {
        if (origin == "department") {
            x = gsub("^D", "", x)
            int = !is.na(suppressWarnings(as.integer(x)))
            if (type == "file") {
                x[int] = sprintf("D%03d", as.integer(x[int]))
                x = gsub("^0*(2[AB])$", "D0\\1", x)
            } else {
                x[int] = sprintf("%02d", as.integer(x[int]))
                x = gsub("^0*(2[AB])$", "\\1", x)
            }
        }
        if (origin == "commune") {
            x[x < "96"] = substr(x[x < "96"], 1, 2)
            x[x > "96"] = substr(x[x > "96"], 1, 3)
            return(format_scope(x, type = type, check = check))
        }
        if (check == TRUE) {
            if (type == "file") {
                admissible = format_scope(
                    floodam.data::department[["code"]],
                    check = FALSE
                )
            } else {
                admissible = floodam.data::department[["code"]]
            }
            x[! x %in% admissible] = as.character(NA)
        }
    }

    if (scope == "region") {
        x = gsub("^R", "", x)
        int = !is.na(suppressWarnings(as.integer(x)))
        if (type == "file") {
            x[int] = sprintf("R%02d", as.integer(x[int]))
        } else {
            x[int] = sprintf("%02d", as.integer(x[int]))
        }
        if (check == TRUE) {
            if (type == "file") {
                admissible = format_scope(
                    floodam.data::region[["code"]],
                    type = "file",
                    scope = "region",
                    check = FALSE
                )
            } else {
                admissible = floodam.data::region[["code"]]
            }
            x[! x %in% admissible] = as.character(NA)
        }
    }

    return(x)
}