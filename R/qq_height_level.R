#' @title qq plot of building heights versus building storeys in the context of 
#' the BD-Topo v3
#' 
#' @description
#' The function produces a QQ-plot that confronts the variable referring to the
#' surface developed and the number of dwellings as issued from the functions 
#' that process the bdtopo. The plot is stored as png image in destination 
#' 
#' @param output_name character.
#' @param dataset data.frame
#' @param destination character. Path to folder where tables and figures must be stocked.
#'
#' 
#' @export 
#'
#' @encoding UTF-8
#' @author David Nortes Martinez

qq_height_level = function(output_name, dataset, destination){
    
    # generating average series
    mean_series = 3 * seq(
        0:max(dataset[["level"]], 
        na.rm = TRUE)
        )

    # color vector
    color = rep(
        scales::alpha("black", 0.35), 
        nrow(dataset)
    )
    color[dataset[["height"]] < 3] = scales::alpha("red",0.4)
    color[dataset[["level"]] == 0] = scales::alpha("dodgerblue", 0.4)
    
    # generating and saving plot in destination
    grDevices::png(
        file.path(
            destination, 
            "figure", 
            paste0(
                output_name, 
                "-", 
                "height-vs-storeys.png"
            )
        ),
        height = 360, 
        width = 720
    )
        graphics::par(mar = c(5,5,4,2))
        plot(
            x = dataset[["height"]],
            y = dataset[["level"]],
            pch = 20,
            cex = 1,
            xlab = "Hauteur du b\u00E2timent (m)",
            ylab = "Nombre d'\u00E9tages",
            col = color,
            cex.lab = 1.8,
            cex.main = 1.8,
            cex.axis = 1.8
        )
        graphics::lines(
            x = mean_series,
            y = seq(0:max(dataset[["level"]], na.rm = TRUE)),
            cex = .2,
            col = "limegreen",
            type = 's'
        )
        graphics::points(
            x = dataset[dataset[["height"]] == 0, "height"],
            y = dataset[dataset[["height"]] == 0, "level"],
            pch = 20,
            cex = 1,
            col = color[dataset[["height"]] == 0]
        )
        graphics::points(
            x = dataset[dataset[["level"]] == 0, "height"],
            y = dataset[dataset[["level"]] == 0, "level"],
            pch = 20,
            cex = 1,
            col = color[dataset[["level"]] == 0]
        )
        graphics::legend(
            "topleft",
            bty = "n",
            lty=c(1,rep(NA,3)),
            pch = c(NA, rep(20,3)),
            col = c(
                "limegreen",
                scales::alpha("black", 0.35), 
                scales::alpha("dodgerblue", 0.4), 
                scales::alpha("red",0.4)),
            legend = format(
                c(
                    "moyenne (3m)",
                    sum(
                        (!is.na(dataset[["height"]] & dataset[["height"]]>0)) & 
                        (!is.na(dataset[["level"]]) & dataset[["level"]]>=3), 
                        na.rm = TRUE
                    ),
                    sprintf(
                        "h. b\u00E2ti. = 0: %i", 
                        sum(
                        (!is.na(dataset[["height"]]) & dataset[["height"]]==0) & 
                        !is.na(dataset[["level"]]), 
                        na.rm = TRUE
                        )
                    ),
                    sprintf(
                        "h. \u00E9tage < 3: %i",
                        sum(
                        !is.na(dataset[["height"]]) & 
                        (!is.na(dataset[["level"]]) & dataset[["level"]] < 3), 
                        na.rm = TRUE
                        )
                    )
                ),
                big.mark = " "
            ),
            cex = 1.8
        )
    grDevices::dev.off()
}