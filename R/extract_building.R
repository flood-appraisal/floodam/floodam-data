#' @title Extract building information from BD TOPO
#' 
#' @description
#' `extract_building()` extracts the layer 'batiment' from BD TOPO.
#' 
#' @details
#' - First `extract_building()` uncompresses the archive '*.7z', typically
#' comming from the use of `download_bd_topo()`.
#' - Then it extracts from 'gpkg file' the 'batiment', 'commune' and
#' 'departement' layers as sf objects, applies scheme to all
#' created sf objects.
#' - A selection of 'polygon' is made in 'batiment', to ensure that only those
#' of current 'department' are kept, and the 'commune' to which they belong is
#' added. The projection is set to 'projection'.
#' - If asked, the eaip information is added. If asked a map for checking
#' purpose is saved.
#' - If asked the resulting sf object is returned (this is not default
#' behaviour).
#' - Result is saved according to 'destination' path.
#'
#' @param origin character, path to the directory where archive are stored.
#' @param destination character, path to the directory where results should be
#' saved.
#' @param archive character, vector of archive to be adpated.
#' @param scheme data.frame, how archive variables should be treated.
#' @param projection numeric, 4 digits projection to use.
#' @param path_eaip character, path where eaip archives should be find.
#' @param journal logical, should a journal file be saved.
#' @param map logical, should a map be plotted (for check purpose).
#' @param verbose logical, should the function give some sumup informations.
#' @param retrieve logical, should the result be returned.
#'
#' @return if retrieve = TRUE, data.frame of adapated data stored in archive.
#'
#' @export
#'
#' @encoding UTF-8

extract_building = function(
    origin,
    destination,
    archive,
    scheme = floodam.data::scheme_bd_topo_3,
    projection = 4326,
    path_eaip = NULL,
    journal = TRUE,
    map = FALSE,
    verbose = TRUE,
    retrieve = FALSE
) {
    ###  Recursive call if archive is missing
    if (missing(archive)) {archive = list.files(origin, pattern = ".7z$")}
    if (length(archive) > 1)
        return(
            invisible(
                lapply(
                    archive,
                    extract_building,
                    origin = origin,
                    destination = destination,
                    scheme = scheme,
                    journal = journal,
                    map = map,
                    path_eaip = path_eaip,
                    projection = projection,
                    retrieve = retrieve,
                    verbose = verbose
                )
            )
        )
    if (length(archive) == 0)
        return(NULL)

    archive = file.path(origin, archive)

    cat(sprintf("'building' extraction from '%s'...\n", basename(archive)))
    old_s2 = suppressMessages(sf::sf_use_s2(FALSE))
    on.exit(suppressMessages(sf::sf_use_s2(old_s2)))

    info = suppressWarnings(analyse_archive(archive))
    building = sprintf(
        "bdtopo-building-%s-%s.rds",
        info[["vintage"]],
        info[["scope"]]
    )
    dir.create(destination, showWarnings = FALSE, recursive = TRUE)

    if (journal == TRUE) {
        journal = file.path(destination, gsub(".rds", ".log", building))
        init_log_info(
            journal,
            main = sprintf("%s in %s", basename(archive), origin),
            treatment = "extract_building",
        )
    } else {
        journal = FALSE
    }

    # Decompression of archive
    path_temp = add_log_info(archive, gpkg_from_7z,
        info  = list(
            "short" = "decompressing",
            "long" = "Decompression of archive"
        ),
        journal = journal, verbose = verbose
    )
    on.exit(unlink(path_temp, recursive = TRUE, force = TRUE), add = TRUE)

    # Reading layers
    result = add_log_info(path_temp, read_gpkg_layer,
        info  = list(
            "short" = "read 'batiment'",
            "long" = "Reading 'batiment' layer"
        ),
        journal = journal, verbose = verbose,
        verification = expression(info_dim(result)),
        layer = "batiment"
    )
    commune = add_log_info(path_temp, read_gpkg_layer,
        info  = list(
            "short" = "read 'commune'",
            "long" = "Reading 'commune' layer"
        ),
        journal = journal, verbose = verbose,
        verification = expression(info_dim(result)),
        layer = "commune"
    )
    department = add_log_info(path_temp, read_gpkg_layer,
        info  = list(
            "short" = "read 'departement'",
            "long" = "Reading 'departement' layer"
        ),
        journal = journal, verbose = verbose,
        verification = expression(info_dim(result)),
        layer = "departement"
    )

    # Applying scheme
    result = add_log_info(result, apply_scheme,
        info  = list(
            "short" = "scheme 'batiment'",
            "long" = "Applying 'batiment' scheme"
        ),
        journal = journal, verbose = verbose,
        scheme = scheme,
        verification = expression(info_dim(result)),
        selection = list(layer = "batiment"), name_origin = "gpkg"
    )
    commune = add_log_info(commune, apply_scheme,
        info  = list(
            "short" = "scheme 'commune'",
            "long" = "Applying 'commune' scheme"
        ),
        journal = journal, verbose = verbose,
        scheme = scheme,
        verification = expression(info_dim(result)),
        selection = list(layer = "commune"), name_origin = "gpkg"
    )
    department = add_log_info(department, apply_scheme,
        info  = list(
            "short" = "scheme 'departement'",
            "long" = "Applying 'departement' scheme"
        ),
        journal = journal, verbose = verbose,
        scheme = scheme,
        verification = expression(info_dim(result)),
        selection = list(layer = "departement"), name_origin = "gpkg"
    )

    # Preparation of building layer
    result = add_log_info(result, add_commune,
        info  = list(
            "short" = "add commune",
            "long" = "Adding commune and department information"
        ),
        journal = journal, verbose = verbose,
        verification = expression(info_dim(result)),
        commune = commune, department = info[["scope"]]
    )
    result = add_log_info(result, sf::st_transform,
        info  = list(
            "short" = "apply projection",
            "long" = "Applying CRS projection"
        ),
        journal = journal, verbose = verbose,
        crs = projection
    )

    # Adding 'eaip' information
    if (!is.null(path_eaip)) {
        result = add_log_info(result, add_eaip,
            info  = list(
                "short" = "add eaip",
                "long" = "Addition of 'eaip' information"
            ),
            journal = journal, verbose = verbose,
            verification = expression(sum(result[['eaip']])),
            path_eaip = path_eaip, department = info[["scope"]]
        )
    }

    # Saving building layer
    add_log_info(result, saveRDS,
        info  = list(
            "short" = "save",
            "long" = "Saving building layer"
        ),
        journal = journal, verbose = verbose,
        verification = expression(object.size(x)),
        file = file.path(destination, building)
    )

    # Plot map
    if (map == TRUE) {
        add_log_info(result, map_log_building,
            info  = list(
                "short" = "plot check map",
                "long" = "Plotting map (check purpose)"
            ),
            journal = journal, verbose = verbose,
            commune = commune, department = department,
            path_map = file.path(destination, gsub(".rds", ".png", building))
        )
    }

    if (retrieve == TRUE) return(invisible(result))
    return(invisible())
}

map_log_building = function(x, commune, department, path_map) {
    grDevices::png(path_map, width = 1000, height = 1000)
    on.exit(grDevices::dev.off())

    dep = format_scope(unique(x[["department"]]), type = "insee")
    commune = sf::st_transform(commune, crs = sf::st_crs(x))
    department = sf::st_transform(department, crs = sf::st_crs(x))
    col = ifelse(
        commune[["department"]] == dep,
        scales::alpha("yellow", .1), scales::alpha("blue", .1)
    )
    plot(
        commune[0],
        col = col,
        border = "gray50",
        cex.axis = 2,
        axes = TRUE,
        xaxt = "n",
        yaxt = "n",
        reset = FALSE
    )
    plot(department[0], col = NA, border = "black", lwd = 2, add = TRUE)
    graphics::axis(
        2,
        at = pretty(sf::st_bbox(commune)[c(2, 4)], 5),
        cex.axis = 2
    )
    graphics::axis(
        4,
        at = pretty(sf::st_bbox(commune)[c(2, 4)], 5),
        cex.axis = 2
    )
    graphics::axis(
        1,
        at = pretty(sf::st_bbox(commune)[c(1, 3)], 5),
        cex.axis = 2
    )
    graphics::axis(
        3,
        at = pretty(sf::st_bbox(commune)[c(1, 3)], 5),
        cex.axis = 2
    )
    graphics::grid()
    # col = as.factor(x[["commune"]]), 
    # col = gray(seq(0.15, 0.85, length.out = length(levels(col))))[col]
    # col = rainbow(length(levels(col)))[col]
    col = ifelse(x[["eaip"]], "red", "black")
    col[is.na(col)] = "black"
    plot(x[0], col = col, border = col, add = TRUE)
    graphics::legend(
        x = "bottomright",
        inset = 1/100,
        legend = c("within eaip", "outside eaip"),
        fill = c("red", "black"),
        cex = 2
    )
    return(NULL)
}
