#' @title Adapt BAN database
#' 
#' @description
#' The following treatments are performed:
#' - data is adapted consistently with scheme (default is `floodam.data::scheme_ban)`
#' - eaip information is added.
#'
#' @param origin character, path to the directory where archive are stored.
#' @param destination character, path to the directory where results should be saved.
#' @param archive character, vector of archive to be adapted.
#' @param scheme data.frame, how archive variables should be treated.
#' @param projection numeric, 4 digits projection to use.
#' @param path_eaip character, path where eaip archives should be found.
#' @param path_admin character, path where admin archives should be found.
#' @param journal logical, should a journal file be saved.
#' @param map logical, should a map be plotted (for checking purposes).
#' @param retrieve logical, should the result be returned.
#' @param verbose logical, should the function give some sumup informations.
#'
#' @return if retrieve = TRUE, data.frame of adapted data stored in archive.
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot

adapt_ban = function(
    origin,
    destination,
    archive,
    scheme = floodam.data::scheme_ban,
    projection = 4326,
    path_eaip = NULL,
    path_admin = NULL,
    map = FALSE,
    journal = TRUE,
    retrieve = FALSE,
    verbose = TRUE
) {
    ###  Recursive call if archive is missing
    if (missing(archive)) {archive = list.files(origin, pattern = ".csv.gz$")}
    if (length(archive) > 1)
        return(
            lapply(
                archive,
                adapt_ban,
                origin = origin,
                destination = destination,
                scheme = scheme,
                projection = projection,
                path_eaip = path_eaip,
                path_admin = path_admin,
                journal = journal,
                map = map,
                retrieve = retrieve,
                verbose = verbose
            )
        )
    if (length(archive) == 0)
        return(NULL)

    # Analyse archive
    archive = file.path(origin, archive)

    message(sprintf("'BAN' adaptation from '%s'...\n", basename(archive)))
    old_s2 = suppressMessages(sf::sf_use_s2(FALSE))
    on.exit(suppressMessages(sf::sf_use_s2(old_s2)))

    info_origin = rev(unlist(strsplit(origin, "/")))
    date_ban = info_origin[1]
    type_ban = info_origin[2]
    dep = strsplit(basename(archive), "-|[.]")[[1]][2]
    destination = file.path(destination, type_ban, date_ban)
    ban = sprintf("%s-%s.rds", type_ban, format_scope(dep))

    dir.create(destination, showWarnings = FALSE, recursive = TRUE)

    if (journal == TRUE) {
        journal = file.path(destination, gsub(".rds", ".log", ban))
        init_log_info(
            journal,
            main = sprintf("%s in %s", basename(archive), origin),
            treatment = "adapt_ban",
        )
    } else {
        journal = FALSE
    }

    # Reading archive with scheme information
    result = add_log_info(
        archive,
        read_with_scheme,
        info  = list(
            "short" = "read 'BAN'",
            "long" = "Reading original 'BAN' archive and applying scheme"
        ),
        journal = journal, verbose = verbose,
        verification = expression(info_dim(result)),
        scheme = scheme,
        projection = projection
    )

    # Adding 'eaip' information
    if (!is.null(path_eaip)) {
        result = add_log_info(result, add_eaip,
            info  = list(
                "short" = "add eaip",
                "long" = "Addition of 'eaip' information"
            ),
            journal = journal, verbose = verbose,
            verification = expression(sum(result[['eaip']])),
            path_eaip = path_eaip, department = dep
        )
    }

    # Plot map
    if (map == TRUE) {
        add_log_info(result, map_log_ban,
            info  = list(
                "short" = "plot check map",
                "long" = "Plotting map (check purpose)"
            ),
            journal = journal, verbose = verbose,
            department = dep,
            path_admin = path_admin,
            path_map = file.path(destination, gsub(".rds", ".png", ban))
        )
    }

    # Saving ban layer
    add_log_info(result, saveRDS,
        info  = list(
            "short" = "save",
            "long" = "Saving ban layer"
        ),
        journal = journal, verbose = verbose,
        verification = expression(object.size(x)),
        file = file.path(destination, ban)
    )

    if (retrieve == TRUE) return(invisible(result))
    return(invisible())
}

map_log_ban = function(x, department, path_admin, path_map) {
    dep = if (missing(department)) {
        format_scope(unique(x[["department"]]), type = "insee")
    } else {
        department
    }
    department = sf::st_transform(
        readRDS(
            file_version(
                path_admin,
                "departement.*rds"
            )
        ),
        crs = sf::st_crs(x)
    )

    commune = file_version(
        path_admin,
        sprintf("commune_%s.*rds", format_scope(dep, type = "file"))
    )

    if (length(commune) == 0) {
        warnings(sprintf("no commune for %s. Nothing is plot.", dep))
        return(NULL)
    } else {
        commune = sf::st_transform(readRDS(commune), crs = sf::st_crs(x))
    }

    grDevices::png(path_map, width = 1000, height = 1000)
    on.exit(grDevices::dev.off())

    col = ifelse(
        commune[["department"]] == dep,
        scales::alpha("yellow", .1), scales::alpha("blue", .1)
    )
    plot(
        commune[0],
        col = col,
        border = "gray50",
        cex.axis = 2,
        axes = TRUE,
        xaxt = "n",
        yaxt = "n"
    )
    graphics::axis(2, at = pretty(sf::st_bbox(commune)[c(2, 4)], 5), cex.axis = 2)
    graphics::axis(4, at = pretty(sf::st_bbox(commune)[c(2, 4)], 5), cex.axis = 2)
    graphics::axis(1, at = pretty(sf::st_bbox(commune)[c(1, 3)], 5), cex.axis = 2)
    graphics::axis(3, at = pretty(sf::st_bbox(commune)[c(1, 3)], 5), cex.axis = 2)
    graphics::grid()
    x = x[order(x[["eaip"]]), ]
    col = ifelse(x[["eaip"]], "red", "gray50")
    col[is.na(col)] = "gray80"
    plot(
        x[0],
        pch = 21,
        cex = .05,
        col = col,
        border = col,
        add = TRUE
    )

    plot(department[0], col = NA, border = "black", lwd = 2, add = TRUE)
    graphics::legend(
        x = "bottomright",
        inset = 1/100,
        legend = c("within eaip", "outside eaip"),
        fill = c("red", "gray50"),
        cex = 2
    )
    return(NULL)
}
