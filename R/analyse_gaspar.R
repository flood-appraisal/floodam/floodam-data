#' @title Analyse catnat theme from gaspar to give some elementary results
#' 
#' @description
#' The following treatments are performed:
#' - catnat declaration are grouped by type of risks, and dates of events. This
#'   leads to event data.frame, for which number of communes concerned are
#'   summed.
#' - From event data.frame, number of events by year, month, and type are
#'   computed.
#' 
#' @param catnat data.frame, data to be analysed
#'
#' @return A list of analysis (catnat, event, year, month, and type)
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot

analyse_catnat = function(catnat) {
    event = stats::aggregate(catnat["commune"], catnat[c("catnat", "date_start", "date_end")], length)
    event = event[order(event[["date_start"]]), ]
    event[["duration"]] = event[["date_end"]] - event[["date_start"]] + 1
    # alert_mattermost(c("**gaspar - last catnat events**", "", as.character(knitr::kable(tail(event, 15), row.names = FALSE))))

    event[["phenomena"]] = NA_character_
    id = 1
    for (i in seq(nrow(event))) {
        max_date = suppressWarnings(max(event[event[["phenomena"]] == id, "date_end"], na.rm = TRUE))
        if (i > 1 && (event[i, "date_start"] - 1 >= max_date + 1)) {
            id = id + 1
        }
        event[i, "phenomena"] = id
    }

    phenomena = do.call(
        rbind,
        mapply(
            function(x){
                result = data.frame(
                    id = unique(x[["phenomena"]]),
                    catnat_type = length(unique(x[["catnat"]])),
                    date_start = min(x[["date_start"]]),
                    date_end = max(x[["date_end"]]),
                    catnat_decree = sum(x[["commune"]])
                )
            },
            split(event, event[["phenomena"]]),
            SIMPLIFY = FALSE
        )
    )

    catnat = merge(
        catnat,
        event[c("catnat", "date_start", "date_end", "duration", "phenomena")]
    )
    phenomena = merge(
        phenomena,
        stats::aggregate(catnat["commune"], catnat["phenomena"], length),
        by.x = "id", by.y = "phenomena"
    )
    phenomena = phenomena[order(as.integer(phenomena[["id"]])), ]
    phenomena[["duration"]] = phenomena[["date_end"]] - phenomena[["date_start"]] + 1
    rownames(phenomena) = NULL

    year = 1982:as.integer(format(Sys.Date(), "%Y"))
    catnat[["year"]] = factor(format(catnat[["date_start"]], "%Y"), levels = year)
    n_year = function(x, year) {
        as.vector(table(factor(format(x[["date_start"]], "%Y"), levels = year)))
    }
    year = data.frame(
        year = year,
        catnat = as.vector(table(catnat[["year"]])),
        commune = stats::aggregate(
            catnat["commune"],
            catnat["year"],
            function(x){length(unique(x))},
            drop = FALSE)[["commune"]],
        event = n_year(event, year),
        phenomena = n_year(phenomena, year)
    )
    year[["commune"]][is.na(year[["commune"]])] = 0

    month = sprintf("%02d", 1:12)
    n_month = function(x, year) {
        as.vector(table(factor(format(x[["date_start"]], "%m"), levels = month)))
    }
    month = data.frame(
        month = month,
        catnat = n_month(catnat, year),
        commune = stats::aggregate(
            catnat["commune"],
            list(month = factor(format(catnat[["date_start"]], "%m"), levels = month)),
            function(x){length(unique(x))},
            drop = FALSE)[["commune"]],
        event = n_month(event, year),
        phenomena = n_month(phenomena, year)
    )
    month[["commune"]][is.na(month[["commune"]])] = 0

    n_type = function(x, year) {as.vector(table(x[["catnat"]]))}
    type = data.frame(
        type = levels(event[["catnat"]]),
        catnat = n_type(catnat),
        event = n_type(event)
    )

    return (
        list(
            catnat = catnat,
            commune = as.matrix(table(catnat[c("year", "commune")])),
            event = event,
            phenomena = phenomena,
            year = year,
            month = month,
            type = type
        )
    )
    
    # plot(year, lwd = 10, col = "grey50", lend = 1) ; graphics::grid()
    # plot(month, lwd = 10, col = "grey50", lend = 1) ; graphics::grid()
}

#' @title Analyse ppr theme from gaspar to give state of documents
#'
#' @description
#' To be added.
#'
#' @param ppr data.frame, data to be analysed
#'
#' @return A factor of state.
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot

analyse_ppr_state = function(ppr) {
    classification = data.frame(
        in_use =
            !is.na(ppr[["date_approval"]]) &
            is.na(ppr[["date_deprescription"]]) &
            is.na(ppr[["date_cancellation"]]) &
            is.na(ppr[["date_repeal"]]),
        in_progress =
            !is.na(ppr[["date_prescription"]]) &
            is.na(ppr[["date_deprescription"]]) &
            is.na(ppr[["date_cancellation"]]) &
            is.na(ppr[["date_repeal"]]) &
            is.na(ppr[["date_approval"]]),
        cancelled =
            !is.na(ppr[["date_deprescription"]]) |
            !is.na(ppr[["date_cancellation"]]) |
            !is.na(ppr[["date_repeal"]]),
        unknown = 
            is.na(ppr[["date_prescription"]]) &
            is.na(ppr[["date_deprescription"]]) &
            is.na(ppr[["date_cancellation"]]) &
            is.na(ppr[["date_repeal"]]) &
            is.na(ppr[["date_approval"]])
    )
    if (any(unique(apply(classification, 1, sum)) != 1)) {
        warning("Classification of pprn has duplicated types.")
    }
    result = rep(NA_character_, nrow(classification))
    result[classification[["unknown"]]] = "unknown"
    result[classification[["cancelled"]]] = "cancelled"
    result[classification[["in_progress"]]] = "progress"
    result[classification[["in_use"]]] = "use"
    return(factor(result, levels = c("use", "progress", "cancelled", "unknown")))
}

#' @title Analyse ppr theme from gaspar to give some elementary results
#'
#' @description
#' The following treatments are performed:
#' - In the element detail, a matrix gives for each commune (rows), the count
#'   of all procedures depending on their state (columns). The column NA is
#'   always given even if, normally, it should stay at 0 for each commune.
#' - In the element summary, from the information in element detail, a state at
#'   the commune is given. It is performed this way: if one document is in state
#'   "use", the commune is in state "use", otherwise if one document is in state
#'   "progress", the commune is in state "progress", otherwise if one document
#'   is in state "cancelled", the commune is in state "cancelled", otherwise if
#'   one document is in state "unknown", the commune is in state "unknown",
#'   otherwise the commune's state is NA.
#' 
#' @param ppr data.frame, data to be analysed
#'
#' @return A list of analysis (detail, summary)
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot

analyse_ppr = function(ppr) {
    result = stats::aggregate(ppr["state"], ppr["commune"], table, useNA = "always")
    detail = result[["state"]]
    rownames(detail) = as.character(result[["commune"]])

    summary = structure(rep(NA_character_, nrow(detail)), names = rownames(detail))
    summary[detail[, "unknown"] > 0] = "unknown"
    summary[detail[, "cancelled"] > 0] = "cancelled"
    summary[detail[, "progress"] > 0] = "progress"
    summary[detail[, "use"] > 0] = "use"

    list(
        detail = detail,
        summary = factor(summary, c("use", "progress", "cancelled", "unknown"))
    )
}