#' @title Download BD TOPO
#'
#' @description
#' Function used to downlaod BD TOPO. It is a wrapper to 
#' `download_archive()`, that helps getting everything fine toi download
#' aimed archive of BD TOPO from IGN websites.
#'
#' @param destination character, the address where dara are stocked.
#' @param origin character, either a keyword or the address from where data are
#' downloaded. See details.
#' @param name character, vector of acceptable names fo archive to be
#' downloaded. Default to "BDTOPO".
#' @param extension character, vector of acceptable types of archive to be
#' downloaded. Default to "7z".
#' @param date character, date of the archive to be downloaded. Default to
#' "last".
#' @param type character, format of GIS data. To be chosen within "SHP", "GPKG",
#' or "SQL". Default to "SHP".
#' @param theme character, themes of BD TOPO to be downloaded when origin is
#' "ign". See details.
#' @param department integer, or converted to integer. Departments that should
#' be considered. See details.
#' @param region integer, or converted to integer. Regions that should be
#' considered. See details.
#' @param verbose logical, should the function send some messages while running.
#'
#' @return nothing
#' 
#' @details
#' If \code{origin == "ign"}, then \bold{origin} is changed to
#' "https://geoservices.ign.fr/bdtopo".
#' 
#' If not, everything shall be filled so that `download_archive()` can make a
#' successful download.
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot
#'
#' @examples
#' \dontrun{
#' destination = tempdir()
#' 
#' # WARNING: download everything that is "new".
#' download_bd_topo(destination) 
#' 
#' download_bd_topo(destination, department = 34)
#' download_bd_topo(destination, department = 34, date = "2019-03-15")
#' download_bd_topo(destination, region = 11, type = "GPKG")
#' 
#' # WARNING: download everything related to D034
#' download_bd_topo(destination, department = 34, date = NULL)
#' 
#' unlink(destination)
#' }

download_bd_topo = function(
    destination,
    origin = "ign", 
    name = "BDTOPO",
    extension = "7z",
    date = "last",
    type = c("SHP", "GPKG", "SQL"),
    theme = c("TOUSTHEMES", "ADMINISTRATIF", "ADRESSES", "BDADRESSE", "ERP", "HYDROGRAPHIE", "TRANSPORT"),
    department = NULL,
    region = NULL,
    verbose = TRUE
) {
    theme = match.arg(theme)
    type = match.arg(type)
    scope = c(format_scope(department), format_scope(region, scope = "region"))
    if (length(scope) > 0 & all(is.na(scope))) {
        message("No relevant department nor region given. NULL is returned.")
        return(NULL)
    }
    scope = scope[!is.na(scope)]
    version = version_archive(precision = theme, type = type, scope = scope)

    if(origin == "ign") {
        origin = "https://geoservices.ign.fr/bdtopo"
    }

    to_do = data.frame(
        origin = origin,
        destination = destination,
        short = "download_bd_topo",
        stringsAsFactors = FALSE
    )

    download_archive(
        to_do,
        extension = extension,
        name = name,
        date = date,
        version = version,
        verbose = verbose
    )
}