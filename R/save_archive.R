#' @title Save an archive in a formatted way
#'
#' @description
#' `save_archive()` saves archives in a formatted way, to help retrieve
#' those archives for a use in a typical 'floodam.data' perspective.
#'
#' @param x data.frame, data to be archived.
#' @param path character, the path from wich the archive's path is constructed.
#' See details.
#' @param scope character or factor. If factor or long character, how x will be
#' sliced. If character of length 1, may be the name of a variable of x. See
#' details.
#' @param extension character, the extension and so the format to be used for
#' saving. See details.
#' @param origin character, default value to "floodam.data". See details.
#'
#' @return Nothin useful.
#'  
#' @details
#' 
#' TO BE COMPLETED
#' 
#' @export
#' 
#' @encoding UTF-8
#' @author Frédéric Grelot
#'
#' @examples
#'
#' x = data.frame(scope = 0:2)
#' temp_dir = tempdir()
#' scope = "scope"
#' 
#' # No scope, no extension, only basename
#' save_archive(x, file.path(temp_dir, "x"))
#' 
#' # Adding a scope
#' save_archive(x, file.path(temp_dir, "x"), scope = "scope")
#' 
#' # Using a path with extension, but also parameter extension
#' save_archive(x, file.path(temp_dir, "x.7z"), extension = "rds", scope = "scope")

save_archive = function(
    x,
    path,
    scope = NULL,
    extension = NULL,
    origin = "floodam.data"
) {
    if (!is.null(scope) & is.atomic(scope) & !is.factor(scope)) {
        scope = as.character(scope)
    }
    if (is.character(scope)) {
        if (length(scope) == 1 && scope %in% names(x)) {
            scope = format_scope(x[[scope]])

        }
        if (length(scope) > 1) {
            scope = as.factor(scope)
        }
    }
    if (is.factor(scope)) {
        if (any(is.na(scope))) {
            warning(
                "Some levels of scope ar NA. ",
                "Corresponding data will not be saved."
            )
        }
        x = split(x, scope)
        return(
            invisible(
                mapply(
                    save_archive,
                    x = x,
                    scope = levels(scope),
                    MoreArgs = list(
                        path = path,
                        extension = extension,
                        origin = origin
                    )
                )
            )
        )
    }
    if (!is.null(scope) & !is.character(scope)) {
        warning("scope should be a character at this stage. It is removed.")
        scope = NULL
    }
    if (nrow(x) == 0) {
        warning("Empty data. Nothing is saved.")
        return(invisible(NULL))
    }

    if (is.null(extension)) {
        extension = basename_ext(path)
        if (extension == "") {
            extension = analyse_archive(path, origin = origin)["extension"]
        }
    }

    if (extension %in% "rds") {
        analyse = analyse_archive(path, origin = origin)
        if (!is.null(scope)) analyse["scope"] = scope
        path = file.path(
            dirname(path),
            format_archive(analyse, extension = extension)
        )
        saveRDS(x, path)
        return(invisible(NULL))
    }

    warning("Unmanaged extension. Nothing is saved.")
    return(invisible(NULL))
}