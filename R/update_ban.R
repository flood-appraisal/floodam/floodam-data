#' @title Update information based on a version of a BAN according to another
#' version of BAN
#'
#' @description
#' `update_ban()` checks consistency between versions of BAN, based on "id" and
#' "address". If "address" is not present in some version, a call to
#' `format_adress()` is performed. For consistent observations, "ban_version" is
#' updated to the final version. For inconsistent observations, it tries to
#' create some updates.
#' 
#' Firste, if an observation with consistent "id" is found, "address" is
#' updated. Secondly, if an observation with consistent "address" is found,
#' "id" is updated. For those automatic corrections, "ban_version" is updated. 
#' 
#' Remaining inconsistent observations are kept as is. They are also extracted
#' in a dataset to help manual corrections to be performed.
#' 
#' @details
#' To be specified.
#'
#' @param ban_i dataframe, initial version of BAN data.
#' @param ban_f dataframe, final version of BAN data.
#' @param v_i character, version of the initial BAN.
#' @param v_f character, version of the final BAN.
#' @param correction dataframe, corrections that remain to be performed
#' manually.
#' @param id_ban_i character, column that is to be used as id in ban_i.
#' @param id_ban_f character, column that is to be used as id in ban_f.
#' 
#' @return List with updated ban_i (data.frame), manual corrections to be
#' performed (data.frame), and summary of changes made (vector).
#' 
#' @export
#' 
#' @encoding UTF-8

update_ban = function(
    ban_i,
    ban_f,
    v_i,
    v_f,
    correction,
    id_ban_i = "id",
    id_ban_f = id_ban_i
) {
    if (!"address" %in% names(ban_i)) {
        ban_i[["address"]] = format_address(ban_i)
    }
    if (!"address" %in% names(ban_f)) {
        ban_f[["address"]] = format_address(ban_f)
    }

    if (id_ban_i != id_ban_f) {
        ban_f[[id_ban_i]] = ban_f[[id_ban_f]]
        ban_f[[id_ban_f]] = NULL
    }
    id = id_ban_i
    id_cor = sprintf("%s_cor", id)

    result = ban_i
    result["id_order"] = seq(nrow(result))

    if (!missing(correction)) {
        result = merge(
            result,
            correction,
            all.x = TRUE,
            by = c(id, "address")
        )
        sel_man = !is.na(result[[id_cor]]) & !is.na(result[["address_cor"]])
        result[sel_man, id] = result[sel_man, id_cor]
        result[sel_man, "address"] = result[sel_man, "address_cor"]
        manual = sum(sel_man)
    } else {
        manual = 0
    }

    log = c("correct" = nrow(merge(result, ban_f, by = c(id, "address"))))

    # Treatment of address with ID found in ban_f
    result = merge(
        result,
        ban_f,
        all.x = TRUE,
        by = id,
        suffixes = c("", "_id")
    )
    sel_id = !is.na(result[["address_id"]])
    result[sel_id, "address"] = result[sel_id, "address_id"]
    log = c(log, "id" = sum(sel_id) - stats::setNames(log, NULL))

    # Treatment of ID with address found in ban_f
    result = merge(
        result,
        ban_f,
        all.x = TRUE,
        by = "address",
        suffixes = c("", "_ad")
    )
    id_ad = sprintf("%s_ad", id)
    sel_ad = is.na(result[["address_id"]]) & !is.na(result[[id_ad]])
    result[sel_ad, id] = result[sel_ad, id_ad]
    log = c(log, address = sum(sel_ad))

    # Creation of remaining corrections
    result = result[order(result[["id_order"]]), ]
    sel_cor  = is.na(result[["address_id"]]) & is.na(result[[id_ad]])
    correction = result[sel_cor, colnames(ban_i)]
    log = c(log, "correction" = sum(sel_cor), "manual" = manual)
    class(log) =  "integer"

    # Adding version in result
    result = result[colnames(ban_i)]
    result[["ban_version"]] = ifelse(
        sel_cor,
        as.character(result[["ban_version"]]),
        v_f
    )

    return(list(adapted = result, correction = correction, log = log))
}