#' @title Apply a scheme to data
#' 
#' @description
#' The function applies a scheme to a data.frame or a sf object, which consists
#' in keeping only variables that should be kept, renaming variables, and
#' setting the type of variables.
#'
#' @param x data.frame or sf, data to be adapted.
#' @param scheme data.frame, scheme to be applied.
#' @param selection list, for big scheme a way to select entries. See details
#' @param name_origin character, when different 'name_origin' are provided, a
#' way to select the good one.
#'
#' @return data adapted, keeping geometry for 'sf' objects.
#' 
#' @details 
#' 
#' 'selection' should be a named list of vector, the name giving the column
#' where to perform the selection.
#' 
#' 'name_origin' should be a character pattern that will help to select between
#' different possibilities in column names starting with "name_origin" in
#' scheme. The simplest, the best.
#'
#' @export
#'
#' @encoding UTF-8

apply_scheme = function(x, scheme, selection = NULL, name_origin = NULL) {
    if (!is.null(name_origin)) {
        name_origin = grep(
            name_origin,
            grep("name_origin", names(scheme), value = TRUE),
            value = TRUE)
        if (length(name_origin) != 1) stop("Bad name_origin")
    } else {
        name_origin = "name_origin"
    }

    if (!is.null(selection)) {
        if (!all(names(selection) %in% names(scheme))) {
            stop("Bad names for selection")
        }
        for (what in names(selection)) {
            scheme = scheme[scheme[[what]] %in% selection[[what]], ]
        }
    }

    scheme = scheme[scheme[["keep"]], ]

    x = x[scheme[[name_origin]]]
    names(x) = c(scheme[["name"]], attr(x, "sf_column"))

    if ("sf" %in% class(x)) {
        geometry = sf::st_geometry(x)
        x = sf::st_drop_geometry(x)
    } else {
        geometry = NULL
    }

    selection = scheme[["name"]][scheme[["type"]] == "factor"]
    x[selection] = lapply(x[selection], as.factor)

    selection = scheme[["name"]][scheme[["type"]] == "Date"] 
    x[selection] = lapply(x[selection], as.Date)

    selection = scheme[["name"]][scheme[["type"]] == "numeric"] 
    x[selection] = lapply(x[selection], as.numeric)

    selection = scheme[["name"]][scheme[["type"]] == "integer"] 
    x[selection] = lapply(x[selection], as.integer)

    selection = scheme[["name"]][scheme[["type"]] == "logical"] 
    x[selection] = lapply(x[selection], to_logical)

    if (!is.null(geometry)) {
        x = sf::st_set_geometry(x, geometry)
    }

    return(x)
}
