#' @title Add transparency to color
#'
#' @description
#' This functions add (or set) transparency to a vector of colors.
#' 
#' @details 
#' Both 'x' and 'alpha' may be vectors. In case their length are not compatible,
#' a vector of the maximum length will be generated without any warning.
#'
#' @param x vector of original color.
#' @param alpha numeric between O and 1, desired transparency.
#' @param maxColorValue integer, same as in rgb.
#'
#' @return vector of colors with alpha.
#' 
#' @export
#' 
#' @encoding UTF-8
#' @author Frédéric Grelot
#'
#' @examples
#' 
#' add_transparency("black", .5)
#' add_transparency(c("red", "black"), .5)
#'
#' # No warnings when length differs
#' add_transparency(c("#000000FF", "black", "red", "blue"), c(0, .5, 1))

add_transparency = function(x, alpha = 0, maxColorValue = 255) {
    mat_rgb = grDevices::col2rgb(x)
    grDevices::rgb(
        mat_rgb[1, ], mat_rgb[2, ], mat_rgb[3, ],
        round(alpha * maxColorValue),
        maxColorValue = maxColorValue,
        names = names(x)
    )
}