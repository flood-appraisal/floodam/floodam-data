## In this file are grouped function that are used but should not be exported

#' @title Add NA for a given value
#'
#' @param x vector
#' @param value value that shall be replaced by NA
#'
#' @return vector
#' 
#' @encoding UTF-8
#' @author Victor Champonnois et Frédéric Grelot
#'
#' @examples
#' x = sample(c("red", "blue"), 10, replace = TRUE)
#' floodam.data:::treat_na(x, "red")
#' floodam.data:::treat_na(x, "blue")
#' floodam.data:::treat_na(x, "green")
 
treat_na = function(x, value) {
    na_value = switch(
        EXPR = typeof(x),
        "character" = NA_character_,
        "integer" = NA_integer_,
        "double" = NA_real_,
        NA
    )
    data.table::fifelse(x == value, na_value, x)
}

#' @title Add information to a journal
#'
#' @description
#' Use a modality for TRUE and onther for FALSE
#'
#' @param ... zero or more objects which can be coerced to character.
#' @param journal character vector, journal
#' @param verbose logical, should x generates a diagnostic message
#' @param first logical, should ... be considered as a first message or a second one preceeded by tabulation
#'
#' @return updated journal
#'
#' @encoding UTF-8
#' @author Frédéric Grelot
#'
#' @examples
#' journal = floodam.data:::add_journal_old("Operation started.")
#' floodam.data:::add_journal_old("No problem occurred.", journal = journal)
#' floodam.data:::add_journal_old("No problem occurred.", journal = journal, verbose = TRUE)
 
add_journal_old = function(..., journal = character(), verbose = FALSE, first = FALSE) {
    args = as.character(unlist(list(...)))
    if (identical(verbose, TRUE)) {
        if (identical(first, FALSE)) message(paste("\t...", args)) else message(args)
    }
    if (identical(first, FALSE)) args = paste("\t", args, sep = "") else args = c(args)
    return(c(journal, args))
}

file_version = function(path, pattern, version = "last") {
    if (is.na(pattern)) return(character())
    decreasing = switch(
        version,
        "old" = FALSE,
        TRUE
    )
    result = utils::head(
        sort(
            list.files(path, pattern, full.names = TRUE, recursive = TRUE),
            decreasing = decreasing
        ),
        1
    )
    return(result)
}

#' @title Get parent url from an url
#' 
#' @description
#' `getbase_url()` gets the base parent from an url.
#' 
#' @details
#' `get_base_url()` keeps unchanged links that do not start exactlty by
#' "http://" or "https://".
#' 
#' @param url character, address of the url.
#' 
#' @export
#' 
#' @examples
#' 
#' get_base_url("http://parent/child/archive.csv.gz")
#' get_base_url("https://parent/child/archive.csv.gz")
#' get_base_url("parent/child/archive.csv.gz")

get_base_url = function(url) {
    pattern = "(https?://[^/]+)/.*"
    gsub(pattern, "\\1", url)
}

#' @title Subset with message
#' 
#' @description
#' `subset_with_message()` makes subset of data.frame that is suitable for
#' a processing of data.
#' 
#' @details 
#' `subset_with_message()` sends message when:
#' - `rows` is not logical because it is an obligation for this function.
#' - `columns` is not a character vector of valid column names, because it is an
#' obligation for this function.
#' - `rows` conducts to delete some observations.
#' 
#' @param x data.frame.
#' @param rows logical, observations to be kept.
#' @param columns character, variables to be kept.
#' 
#' @return `data.frame` of selected observations and variables.
#' 
#' @export

subset_with_message = function(x, rows, columns) {
    if (missing(rows)) rows = rep(TRUE, nrow(x))
    if (missing(columns)) columns = colnames(x)
    if (!is.logical(rows)) {
        message("- rows must be logical. No subset performed on observations.")
        rows = rep(TRUE, nrow(x))
    }
    if (!is.character(columns) || !all(columns %in% names(x))) {
        message(
            "- columns must be valid column names. ",
            "No subset performed on variables"
        )
        columns = names(x)
    }
    if (sum(rows) < nrow(x)) {
        message(
            sprintf(
                "- %s observations not in selection (removed).",
                sum(!rows)
            )
        )
    }
    return(x[rows, columns, dop = FALSE])
}

#' @title Transform a vector in a logical ones
#' 
#' @description
#' `as_logical()` enhances `as.logical()`, especially when input is a
#' character, by treating some comon cases, at least in English and French
#' context.
#' 
#' @details
#' When applied to a character vector, `as_logical()` tests if all values are
#' in a set of admissibles values for `TRUE` cases, `FALSE` cases, and `NA`
#' cases. If it is the case, it makes all substitutions so that a `logical`
#' vector is returned by `as.logical()`.
#' 
#' Some defaults cases are implemented (all insensitive to case)
#' 
#' - English TRUE/FALSE with:
#'      - `TRUE` for "T" and "TRUE"
#'      - `FALSE` for "F" and "FALSE"
#' - English YES/NO with:
#'      - `TRUE` for "Y" and "YES"
#'      - `FALSE` for "N" and "NO"
#' - French TRUE/FALSE with:
#'      - `TRUE` for "V" and "VRAI"
#'      - `FALSE` for "F" and "FAUX"
#' - French YES/NO with:
#'      - `TRUE` for "O" and "OUI"
#'      - `FALSE` for "N" and "NON"
#' 
#' `true` and `false` can be used to add more cases. To be used they must be
#' both given, if not none is considered. Even if `true` and `false` are given,
#' default substitutions for French and English cases are performed.
#'
#' @param x vector, values to be transformed.
#' @param true character, optional values for `TRUE` cases.
#' @param false character, optional values for `FALSE` cases.
#' @param as_is logical, should result be sent as character (for recursive
#' call).
#'
#' @return logical
#' 
#' @encoding UTF-8
#' 
#' @export
#'
#' @examples
#' 
#' # Default behaviour
#' to_logical(c(0:2, NA))
#' 
#' # For English (default, case insensitive)
#' to_logical(c("true", "false", "t", "f", "True", "False", "", "NA", NA))
#' to_logical(c("y", "n", "Yes", "No"))
#' 
#' # For French (default)
#' to_logical(c("n", "o", "Non", "Oui"))
#' to_logical(c("v", "f", "vrai", "faux"))
#' 
#' # For other convention (language)
#' x = c("si", "no", NA)
#' to_logical(x)
#' to_logical(x, true = "SI", false = "NO")
#'
#' # Cannot mix language (by default)
#' x = c("NON", "OUI", "YES", "NO")
#' to_logical(x)
#' 
#' # Can use parameters in this case
#' to_logical(x, true = c("OUI", "YES"), false = c("NO", "NON"))

to_logical = function(x, true, false, as_is = FALSE) {
    if (is.character(x)) {
        x = toupper(x)
        na = c("", "NA", NA)
        if (!missing(true) & !(missing(false))) {
            true = toupper(true)
            false = toupper(false)
            if (all(x %in% c(true, false, na))) {
                x[x %in% true] = TRUE
                x[x %in% false] = FALSE
            }
            if (isTRUE(as_is)) {
                return(x)
            }
        } else {
            x = to_logical(x, c("y", "yes"), c("n", "no"), TRUE)
            x = to_logical(x, c("o", "oui"), c("n", "non"), TRUE)
            x = to_logical(x, c("v", "vrai"), c("f", "faux"), TRUE)
        }
    }
    as.logical(x)
}
