#' @title Analyse intersections between spatial DBs
#' 
#' @description
#' The following treatments are performed:
#' - data is adapated consistently with theme
#' - only data corresponding to flood hazards are kept
#' - if scope is given, a subpart of data is also saved
#'
#' @param x sf object, spatial DB that is analysed.
#' @param y sf object, spatial DB that is used for the analysis.
#' @param eaip sf objectf, eaip DB that is used for the analysis.
#' @param analysis character, analyses that is performed. Default to
#' \code{c("intersect", "proportion")}. See details.
#' @param verbose logical, should the function gives some messages. Default to*
#' TRUE.
#'
#' @return
#' 
#' analyse_intersection returns a data.frame, whose columns contain analyses
#' performed.
#' 
#' analyse_eaip returns a sf object, basically x with added columns for each
#' analysis performed.
#'
#' @export
#'
#' @encoding UTF-8
#' @author Frédéric Grelot
#' 
#' @examples
#' 
#' \dontrun{
#' origin = "data-local/georisques/gaspar"
#' adapt_gaspar(file.path(origin))
#' adapt_gaspar(file.path(origin, "2022-02-28"), scope = list("so_ii" = so.ii::so_ii_scope))
#' }

analyse_intersection = function(
    x,
    y,
    analysis = c("intersect", "proportion"),
    verbose = TRUE
) {
    old_sf_use_s2 = sf::sf_use_s2(FALSE)
    on.exit(sf::sf_use_s2(old_sf_use_s2))

    result = sf::st_drop_geometry(x)[FALSE]
    if ("intersect" %in% analysis) {
        intersect = suppressMessages(
            suppressWarnings(
                sf::st_intersects(sf::st_geometry(x), sf::st_geometry(y))
            )
        )
        result[["intersect"]] = lengths(intersect) > 0
    }
    if ("proportion" %in% analysis) {
        x[["id"]] = seq(nrow(x))
        proportion = suppressMessages(
            suppressWarnings(            
                sf::st_intersection(x["id"], sf::st_geometry(y))
            )
        )
        id = factor(proportion[["id"]], x[["id"]])
        proportion = stats::aggregate(
            sf::st_area(proportion),
            list(id = id),
            sum,
            drop = FALSE
        )[["x"]]
        proportion[is.na(proportion)] = 0
        result[["proportion"]] = pmin(
            units::drop_units(proportion / sf::st_area(x)),
            1
        )
    }
    if (ncol(result) == 0) return(NULL)
    return(result)
}

#' @describeIn analyse_intersection Analyse intersections between spatial DB
#' and eaip DB
analyse_eaip = function(
    x,
    eaip,
    analysis = "intersect",
    verbose = TRUE
) {
    result = analyse_intersection(x, eaip, analysis, verbose)
    if ("intersect" %in% names(result)) x["eaip"] = result["intersect"]
    if ("proportion" %in% names(result)) x["eaip_prop"] = result["proportion"]
    return(x)
}